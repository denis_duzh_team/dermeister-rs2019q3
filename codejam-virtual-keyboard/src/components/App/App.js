import Component from '../Component';
import Keyboard from '../Keyboard/Keyboard';
import Input from '../Input/Input';

export default class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      action: '',
      value: '',
    };

    this.input = new Input(() => ({ action: this.state.action, value: this.state.value }));
    this.appendComponent(this.input);

    this.keyboard = new Keyboard(() => ({
      onChar: (value) => this.setState({ value, action: 'CHAR' }),
      onNavigation: (navigation) => this.setState({ value: navigation, action: 'NAVIGATION' }),
    }));
    this.appendComponent(this.keyboard);
  }
}
