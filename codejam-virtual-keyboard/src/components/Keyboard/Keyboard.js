import Component from '../Component';
import CharButton from '../CharButton/CharButton';
import ControlButton from '../ControlButton/ControlButton';
import NavigationButton from '../NavigationButton/NavigationButton';

import buttons from './buttons.json';
import './Keyboard.scss';

export default class Keyboard extends Component {
  constructor(props) {
    super(props);
    this.element = document.createElement('ul');

    this.state = {
      isUpperCase: false,
      lang: localStorage.getItem('language') || 'ENGLISH',
      pressedButtons: new Set(),
    };

    this.initKeyboardListeners();
    this.initButtons();
  }

  initKeyboardListeners() {
    document.addEventListener('keydown', (evt) => {
      evt.preventDefault();

      this.setState((state) => {
        const { pressedButtons } = state;
        pressedButtons.add(evt.code);

        return {
          ...state,
          pressedButtons,
        };
      });
      this.detectShortcut();
    });

    document.addEventListener('keyup', (evt) => {
      evt.preventDefault();

      this.setState((state) => {
        const { pressedButtons } = state;
        pressedButtons.delete(evt.code);

        return {
          ...state,
          pressedButtons,
        };
      });
    });
  }

  initButtons() {
    buttons.forEach((button) => {
      let buttonComponent;

      switch (button.type) {
        case 'CHAR':
          buttonComponent = new CharButton(() => ({
            config: button,
            language: this.state.lang,
            isUpperCase: this.state.isUpperCase,
            isPressed: this.state.pressedButtons.has(button.code),
            onclick: (value) => {
              if (this.props) this.props().onChar(value);
            },
          }));

          break;
        case 'NAVIGATION':
          buttonComponent = new NavigationButton(() => ({
            isPressed: this.state.pressedButtons.has(button.code),
            title: button.title,
            code: button.code,
            onclick: (navigation) => {
              if (this.props) this.props().onNavigation(navigation);
            },
          }));

          break;
        case 'CONTROL':
          buttonComponent = new ControlButton(() => ({
            title: button.title,
            isPressed: this.state.pressedButtons.has(button.code),
            ...this.controlClickFns(button.code),
          }));

          break;
        default:
      }

      this.appendComponent(buttonComponent);
    });
  }

  controlClickFns(code) {
    const changeCase = (state) => ({ ...state, isUpperCase: !state.isUpperCase });

    let clickFns;
    switch (code) {
      case 'ShiftLeft':
      case 'ShiftRight':
        clickFns = {
          onmousedown: () => this.setState(changeCase),
          onmouseup: () => this.setState(changeCase),
        };

        break;
      case 'CapsLock':
        clickFns = {
          onclick: () => this.setState(changeCase),
        };

        break;
      default:
    }

    return clickFns;
  }

  detectShortcut() {
    const pressedButtons = [...this.state.pressedButtons].sort().toString();
    const changeLangShortcuts = ['AltLeft,ShiftLeft', 'AltLeft,CapsLock,ShiftLeft'];

    if (changeLangShortcuts.some((shortcut) => shortcut === pressedButtons)) {
      this.setState((state) => {
        const lang = state.lang === 'RUSSIAN' ? 'ENGLISH' : 'RUSSIAN';
        localStorage.setItem('language', lang);

        return {
          ...state,
          lang,
        };
      });
    }
  }

  render() {
    this.element.classList.add('keyboard');
    return this.element;
  }
}
