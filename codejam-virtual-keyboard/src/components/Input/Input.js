import Component from '../Component';

import './Input.scss';

export default class Input extends Component {
  constructor(props) {
    super(props);

    this.element = document.createElement('textarea');
  }

  backspace() {
    this.element.textContent = this.element.textContent.slice(0, -1);
    this.element.selectionStart = this.element.textContent.length;
  }

  newLine() {
    this.element.textContent += '\n';
    this.element.selectionStart = this.element.textContent.length;
  }

  char(value) {
    this.element.textContent += value;
    this.element.selectionStart = this.element.textContent.length;
  }

  performNavigation(navigation) {
    this.element.focus();

    switch (navigation.type) {
      case 'NEW_LINE':
        this.newLine();
        break;
      case 'BACKSPACE':
        this.backspace();
        break;
      default:
    }
  }

  render() {
    let props = {};
    if (this.props) props = this.props();

    this.element.classList.add('input');

    switch (props.action) {
      case 'CHAR':
        this.char(props.value);
        break;

      case 'NAVIGATION':
        this.performNavigation(props.value);
        break;

      default:
        this.element.textContent = '';
    }

    return this.element;
  }
}
