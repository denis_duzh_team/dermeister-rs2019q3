import Component from '../Component';

import './CharButton.scss';

export default class CharButton extends Component {
  constructor(props) {
    super(props);

    this.isPressed = false;
    this.element = document.createElement('li');
  }

  render() {
    let props = {};
    if (this.props) props = this.props();

    const language = props.config[props.language];
    const letter = props.isUpperCase ? language.shift : language.default;

    this.element.classList.add('char-button');
    this.element.textContent = props.config.title || letter;
    this.element.onclick = () => props.onclick(letter);

    if (props.isPressed && !this.isPressed) {
      this.isPressed = true;
      this.element.classList.add('char-button--pressed');

      if (props.onclick) props.onclick(letter);
    } else if (!props.isPressed && this.isPressed) {
      this.isPressed = false;
      this.element.classList.remove('char-button--pressed');
    }

    return this.element;
  }
}
