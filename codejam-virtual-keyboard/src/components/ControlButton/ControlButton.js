import Component from '../Component';

import './ControlButton.scss';

export default class ControlButton extends Component {
  constructor(props) {
    super(props);
    this.element = document.createElement('li');

    this.isPressed = false;
  }

  render() {
    let props = {};
    if (props) props = this.props();

    this.element.classList.add('control-button');
    this.element.textContent = props.title;
    this.element.onclick = props.onclick;
    this.element.onmouseup = props.onmouseup;
    this.element.onmousedown = props.onmousedown;

    if (props.isPressed && !this.isPressed) {
      this.isPressed = true;
      if (props.onmousedown) props.onmousedown();
      if (props.onclick) props.onclick();

      this.element.classList.add('control-button--pressed');
    } else if (!props.isPressed && this.isPressed) {
      this.isPressed = false;
      if (props.onmouseup) props.onmouseup();
      if (props.onclick) props.onclick();

      this.element.classList.remove('control-button--pressed');
    }

    return this.element;
  }
}
