export default class Component {
  constructor(props) {
    this.props = props;
    this.state = {};
    this.components = [];

    this.element = document.createElement('div');
  }

  setState(state) {
    if (typeof state === 'function') this.state = state(this.state);
    else this.state = { ...this.state, ...state };

    this.components.forEach((component) => component.render());
    this.render();
  }

  render() {
    return this.element;
  }

  appendComponent(component) {
    this.components.push(component);
    this.element.appendChild(component.render());
  }
}
