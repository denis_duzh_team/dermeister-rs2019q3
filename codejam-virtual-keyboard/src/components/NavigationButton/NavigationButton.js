import Component from '../Component';

import './NavigationButton.scss';

export default class NavigationButton extends Component {
  constructor(props) {
    super(props);

    this.element = document.createElement('li');

    this.isPressed = false;
  }

  render() {
    let props = {};
    if (props) props = this.props();

    this.element.classList.add('navigation-button');
    this.element.textContent = props.title;

    let navigation;

    switch (props.code) {
      case 'Backspace':
        navigation = { type: 'BACKSPACE' };
        break;

      case 'Enter':
        navigation = { type: 'NEW_LINE' };
        break;

      case 'ArrowLeft':
        navigation = { type: 'MOVE_CURSOR', value: 'LEFT' };
        break;

      case 'ArrowDown':
        navigation = { type: 'MOVE_CURSOR', value: 'DOWN' };
        break;

      case 'ArrowUp':
        navigation = { type: 'MOVE_CURSOR', value: 'UP' };
        break;

      case 'ArrowRight':
        navigation = { type: 'MOVE_CURSOR', value: 'RIGHT' };
        break;
      default:
    }

    this.element.onclick = () => props.onclick(navigation);

    if (props.isPressed && !this.isPressed) {
      this.isPressed = true;
      this.element.classList.add('navigation-button--pressed');
      props.onclick(navigation);
    } else if (!props.isPressed && this.isPressed) {
      this.isPressed = false;
      this.element.classList.remove('navigation-button--pressed');
    }

    return this.element;
  }
}
