const isValid = (matrix, i, j, value) => {
  const row = matrix[i];
  const column = matrix.map(row => row[j]);
  const area = [];
  const iBound = Math.floor(i / 3) * 3;
  const jBound = Math.floor(j / 3) * 3;
  for (let i = iBound; i < iBound + 3; i++)
    for (let j = jBound; j < jBound + 3; j++) area.push(matrix[i][j]);

  return !row.includes(value) && !column.includes(value) && !area.includes(value);
};

module.exports = function solveSudoku(matrix) {
  const zeros = [];
  const moves = [];

  for (let i = 0; i < 9; i++)
    for (let j = 0; j < 9; j++) if (matrix[i][j] === 0) zeros.unshift([i, j]);

  while (zeros.length > 0) {
    let [i, j] = zeros.pop();
    let value = matrix[i][j];

    do {
      value++;
    } while (!isValid(matrix, i, j, value) && value <= 9);

    if (value <= 9) {
      matrix[i][j] = value;
      moves.push([i, j]);
    } else {
      matrix[i][j] = 0;
      zeros.push([i, j], moves.pop());
    }
  }

  return matrix;
};
