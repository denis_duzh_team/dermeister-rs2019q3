import { CANVAS_SIZE, imageDataToDataURI, drawImage, initImage } from './Utils';

export const setTool = (tool) => localStorage.setItem('tool', tool);
export const getTool = () => localStorage.getItem('tool') || 'PENCIL';

export const setPixelSize = (pixelSize) => localStorage.setItem('pixelSize', pixelSize);
export const getPixelSize = () => Number(localStorage.getItem('pixelSize')) || 1;

export const setCurrentColor = (color) => localStorage.setItem('currentColor', JSON.stringify(color));
export const getCurrentColor = () => JSON.parse(localStorage.getItem('currentColor')) || [255, 0, 0, 255];

export const setPreviousColor = (color) => localStorage.setItem('previousColor', JSON.stringify(color));
export const getPreviousColor = () => JSON.parse(localStorage.getItem('previousColor')) || [255, 0, 0, 255];

export const getImageData = () => {
  const imageData = localStorage.getItem('imageData');
  return imageData !== null ? drawImage(imageData) : initImage(CANVAS_SIZE);
};
export const setImageData = (imageData) => localStorage.setItem('imageData', imageDataToDataURI(imageData));

export const getImageLoaded = () => localStorage.getItem('imageLoaded') || false;
export const setImageLoaded = (imageLoaded) => localStorage.setItem('imageLoaded', imageLoaded);

export const getUser = () => localStorage.getItem('user');
export const setUser = (user) => localStorage.setItem('user', user);
export const removeUser = () => localStorage.removeItem('user');

export const tools = [
  { toolId: 'PAINT_BUCKET', title: 'Paint bucket', className: 'paint-bucket' },
  { toolId: 'CHOOSE_COLOR', title: 'Choose color', className: 'choose-color' },
  { toolId: 'PENCIL', title: 'Pencil', className: 'pencil' },
];

export const colors = [
  { value: [247, 41, 41, 255], title: 'Red', className: 'red' },
  { value: [0, 188, 212, 255], title: 'Blue', className: 'blue' },
];

export const pixelSizes = [1, 2, 4];
