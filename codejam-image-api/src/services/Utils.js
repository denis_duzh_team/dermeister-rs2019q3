export const CANVAS_SIZE = 512;

const normalizeCoords = (absoluteX, absoluteY, pixelSize) =>
  [absoluteX, absoluteY].map((value) => pixelSize * Math.floor(value / pixelSize));

export const colorsEqual = ([r1, g1, b1, a1], [r2, g2, b2, a2]) => r1 === r2 && g1 === g2 && b1 === b2 && a1 === a2;

const validateCoords = (x, y) => x >= 0 && x < CANVAS_SIZE && y >= 0 && y < CANVAS_SIZE;

export const getColorAt = (imageData, x, y) => {
  if (validateCoords(x, y)) {
    const r = imageData[CANVAS_SIZE * 4 * y + x * 4];
    const g = imageData[CANVAS_SIZE * 4 * y + x * 4 + 1];
    const b = imageData[CANVAS_SIZE * 4 * y + x * 4 + 2];
    const a = imageData[CANVAS_SIZE * 4 * y + x * 4 + 3];

    return [r, g, b, a];
  }
  return [];
};

export const drawPixel = (imageData, [r, g, b, a], pixelSize, absoluteX, absoluteY) => {
  if (validateCoords(absoluteX, absoluteY)) {
    const [x, y] = normalizeCoords(absoluteX, absoluteY, pixelSize);

    for (let i = 0; i < pixelSize; i += 1) {
      for (let j = 0; j < pixelSize; j += 1) {
        imageData[CANVAS_SIZE * 4 * (y + i) + (x + j) * 4] = r;
        imageData[CANVAS_SIZE * 4 * (y + i) + (x + j) * 4 + 1] = g;
        imageData[CANVAS_SIZE * 4 * (y + i) + (x + j) * 4 + 2] = b;
        imageData[CANVAS_SIZE * 4 * (y + i) + (x + j) * 4 + 3] = a;
      }
    }
  }

  return imageData;
};

const calculateLineFunction = (f, y, x, dy, dx, signY, signX) => {
  f += dy;
  if (f > 0) {
    f -= dx;
    y += signY;
  }
  x -= signX;
  return [f, y, x];
};

export const drawLine = (imageData, color, pixelSize, x0, y0, x1, y1) => {
  const signY = y1 - y0 < 0 ? -1 : 1;
  const signX = x0 - x1 < 0 ? -1 : 1;

  const dy = (y1 - y0) * signY;
  const dx = (x0 - x1) * signX;

  let f = 0;
  let x = x0;
  let y = y0;
  do {
    if (Math.abs(y1 - y0) <= Math.abs(x0 - x1)) {
      [f, y, x] = calculateLineFunction(f, y, x, dy, dx, signY, signX);
    } else {
      [f, x, y] = calculateLineFunction(f, x, y, dx, dy, -1 * signX, -1 * signY);
    }
    imageData = drawPixel(imageData, color, pixelSize, x, y);
  } while (x != x1 || y != y1);

  return imageData;
};

export const floodFill = (imageData, newColor, pixelSize, originX, originY) => {
  const oldColor = getColorAt(imageData, originX, originY);
  [originX, originY] = normalizeCoords(originX, originY, pixelSize);
  const stack = [[originX, originY]];

  if (!colorsEqual(oldColor, newColor)) {
    while (stack.length > 0) {
      const [x, y] = stack.pop();
      imageData = drawPixel(imageData, newColor, pixelSize, x, y);

      if (colorsEqual(getColorAt(imageData, x, y + pixelSize), oldColor)) stack.push([x, y + pixelSize]);
      if (colorsEqual(getColorAt(imageData, x, y - pixelSize), oldColor)) stack.push([x, y - pixelSize]);
      if (colorsEqual(getColorAt(imageData, x + pixelSize, y), oldColor)) stack.push([x + pixelSize, y]);
      if (colorsEqual(getColorAt(imageData, x - pixelSize, y), oldColor)) stack.push([x - pixelSize, y]);
    }
  }

  return imageData;
};

export const drawImage = (imageSrc) => {
  const image = new Image();
  image.crossOrigin = 'anonymous';
  image.src = imageSrc;

  const canvas = document.createElement('canvas');
  canvas.width = CANVAS_SIZE;
  canvas.height = CANVAS_SIZE;
  const ctx = canvas.getContext('2d');

  return new Promise((resolve) => {
    image.onload = () => {
      let { width, height } = image;
      const ratio = width / height;

      if (width > height) {
        width = CANVAS_SIZE;
        height = width / ratio;
      } else {
        height = CANVAS_SIZE;
        width = height * ratio;
      }

      const x = (CANVAS_SIZE - width) / 2;
      const y = (CANVAS_SIZE - height) / 2;

      ctx.drawImage(image, 0, 0, image.width, image.height, x, y, width, height);
      resolve(ctx.getImageData(0, 0, CANVAS_SIZE, CANVAS_SIZE).data);
    };
  });
};

export const toBW = (imageData) => {
  for (let i = 0; i < CANVAS_SIZE; i += 1) {
    for (let j = 0; j < CANVAS_SIZE; j += 1) {
      const [r, g, b, a] = getColorAt(imageData, j, i);
      const average = (r + g + b) / 3;
      const newColor = [average, average, average, a];
      drawPixel(imageData, newColor, 1, j, i);
    }
  }
  return imageData;
};

export const initImage = (size) => Uint8ClampedArray.from(Array(size * size * 4).fill(255));

export const imageDataToDataURI = (imageData) => {
  const canvas = document.createElement('canvas');
  canvas.width = CANVAS_SIZE;
  canvas.height = CANVAS_SIZE;
  const ctx = canvas.getContext('2d');
  ctx.putImageData(new ImageData(imageData, CANVAS_SIZE, CANVAS_SIZE), 0, 0);
  return canvas.toDataURL('image/png');
};

const toHex = (value) => value.toString(16).padStart(2, '0');

const toDecimal = (hex) => parseInt(hex, 16);

export const toRGBA = ([r, g, b, a]) => `rgba(${r}, ${g}, ${b}, ${a})`;

export const toRGB = ([r, g, b]) => `#${toHex(r)}${toHex(g)}${toHex(b)}`;

export const toColor = (color) => {
  const r = toDecimal(color.slice(1, 3));
  const g = toDecimal(color.slice(3, 5));
  const b = toDecimal(color.slice(5));
  return [r, g, b, 255];
};

export const fetchImageSrc = async (city) => {
  const query = `https://api.unsplash.com/photos/random?query=town,${city}&client_id=f25fb0490b7bd09c2c342f226d2cc06a6e81d41375ab531b518edfaad35d1f63`;
  return (await (await fetch(query)).json()).urls.regular;
};
