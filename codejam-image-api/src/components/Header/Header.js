import RS from '../RS';

import './Header.scss';

export default class Header extends RS.Component {
  render() {
    return (
      <header className="header">
        <h1 className="header__heading">CodeJam - Palette</h1>

        <button className="header__menu-toggle">
          <span className="visually-hidden">Toggle menu</span>
        </button>

        <button className="header__options-toggle" onclick={this.props.onLogOut}>
          Log out
        </button>
      </header>
    );
  }
}
