import RS from '../RS';

export default class RadioInput extends RS.Component {
  render() {
    return (
      <div>
        <input
          className="visually-hidden"
          type="radio"
          name={this.props.name}
          id={this.props.id}
          checked={this.props.checked}
          onchange={() => this.props.onchange(this.props.value)}
        />

        <label className={this.props.className} htmlFor={this.props.id}>
          {this.props.label}
        </label>
      </div>
    );
  }
}
