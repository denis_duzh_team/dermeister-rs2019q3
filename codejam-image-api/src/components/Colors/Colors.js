import RS from '../RS';

import { toColor, toRGB, toRGBA } from '../../services/Utils';

import './Colors.scss';

export default class Colors extends RS.Component {
  constructor(props) {
    super(props);

    this.currentColor = RS.createRef();
    this.previousColor = RS.createRef();
  }

  updateCSSProps() {
    this.currentColor.current.style.setProperty('--current-color', toRGBA(this.props.current));
    this.previousColor.current.style.setProperty('--prev-color', toRGBA(this.props.previous));
  }

  componentDidMount() {
    this.updateCSSProps();
  }

  componentDidUpdate() {
    this.updateCSSProps();
  }

  render() {
    return (
      <div className={`colors ${this.props.className}`}>
        <input
          className="colors__picker"
          type="color"
          id="current"
          oninput={(e) => this.props.onColor(toColor(e.target.value))}
          value={toRGB(this.props.current)}
        />
        <label ref={this.currentColor} className="colors__btn colors__btn--current" htmlFor="current">
          Current
        </label>

        <button
          ref={this.previousColor}
          className="colors__btn colors__btn--prev"
          onclick={() => this.props.onColor(this.props.previous)}
        >
          Previous
        </button>

        {this.props.colors.map((color) => (
          <button
            className={`colors__btn colors__btn--${color.className}`}
            onclick={() => this.props.onColor(color.value)}
          >
            {color.title}
          </button>
        ))}
      </div>
    );
  }
}
