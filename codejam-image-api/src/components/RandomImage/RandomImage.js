import RS from '../RS';

import { fetchImageSrc } from '../../services/Utils';

import './RandomImage.scss';

export default class RandomImage extends RS.Component {
  constructor(props) {
    super(props);

    this.state = { city: '', bwPressed: false };
  }

  onBW(e) {
    e.preventDefault();

    if (this.props.imageLoaded) {
      this.props.onBW();
    } else {
      this.setState({ bwPressed: true });
    }
  }

  onChange(e) {
    this.setState({ city: e.target.value });
  }

  onSubmit(e) {
    e.preventDefault();
    fetchImageSrc(this.state.city).then((imageSrc) => this.props.onImageSrc(imageSrc));
  }

  getAlertClass() {
    if (!this.props.imageLoaded && this.state.bwPressed) return 'alert-visible';

    return 'alert-hidden';
  }

  render() {
    return (
      <form className={this.props.className} onsubmit={(e) => this.onSubmit(e)}>
        <button onclick={(e) => this.onBW(e)}>{'B&W'}</button>
        <input value={this.state.city} onchange={(e) => this.onChange(e)} onkeypress={(e) => e.stopPropagation()} />
        <input type="submit" value="Load image" />
        <div className={this.getAlertClass()}>No image has been loaded..</div>
      </form>
    );
  }
}
