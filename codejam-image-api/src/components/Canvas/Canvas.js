import RS from '../RS';

import { CANVAS_SIZE } from '../../services/Utils';

import './Canvas.scss';

export default class Canvas extends RS.Component {
  constructor(props) {
    super(props);

    this.canvas = RS.createRef();
  }

  componentDidMount() {
    this.ctx = this.canvas.current.getContext('2d');
    this.drawImage();
  }

  componentDidUpdate() {
    this.drawImage();
  }

  mouseDown({ offsetX: x, offsetY: y }) {
    this.props.onMouseDown(x, y);
  }

  mouseUp() {
    this.props.onMouseUp();
  }

  mouseMove({ offsetX: x, offsetY: y }) {
    this.props.onMouseMove(x, y);
  }

  drawImage() {
    this.ctx.putImageData(new ImageData(this.props.imageData, CANVAS_SIZE, CANVAS_SIZE), 0, 0);
  }

  render() {
    return (
      <canvas
        ref={this.canvas}
        className={`canvas ${this.props.className}`}
        width={CANVAS_SIZE}
        height={CANVAS_SIZE}
        onmousedown={(e) => this.mouseDown(e)}
        onmouseup={() => this.mouseUp()}
        onmousemove={(e) => this.mouseMove(e)}
        oncontextmenu={() => false}
      >
        Your browser doesn't support canvas
      </canvas>
    );
  }
}
