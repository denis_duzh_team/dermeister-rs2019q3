import RS from '../RS';

import RadioInput from '../RadioInput/RadioInput';

import { CANVAS_SIZE } from '../../services/Utils';

import './Sizes.scss';

export default class Sizes extends RS.Component {
  render() {
    return (
      <div className="sizes">
        {this.props.sizes.map((size) => (
          <RadioInput
            className="sizes__toggle"
            name="sizes"
            id={size}
            checked={this.props.current === size}
            onchange={this.props.onSize}
            value={size}
            label={`${CANVAS_SIZE / size}px`}
          />
        ))}
      </div>
    );
  }
}
