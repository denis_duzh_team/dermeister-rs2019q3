import netlify from 'netlify-auth-providers';
import RS from '../RS';

import Header from '../Header/Header';
import Tools from '../Tools/Tools';
import Colors from '../Colors/Colors';
import Canvas from '../Canvas/Canvas';
import RandomImage from '../RandomImage/RandomImage';
import Sizes from '../Sizes/Sizes';

import {
  getTool,
  setTool,
  getPixelSize,
  setPixelSize,
  getCurrentColor,
  setCurrentColor,
  getPreviousColor,
  getImageData,
  getUser,
  setUser,
  removeUser,
  tools,
  colors,
  pixelSizes,
  setPreviousColor,
  setImageData,
  getImageLoaded,
  setImageLoaded,
} from '../../services/Config';

import {
  drawPixel,
  floodFill,
  drawLine,
  getColorAt,
  colorsEqual,
  drawImage,
  toBW,
  initImage,
  CANVAS_SIZE,
} from '../../services/Utils';

import './App.scss';

export default class App extends RS.Component {
  constructor(props) {
    super(props);

    this.state = {
      tool: getTool(),
      currentColor: getCurrentColor(),
      previousColor: getPreviousColor(),
      imageData: initImage(CANVAS_SIZE),
      pixelSize: getPixelSize(),
      imageLoaded: getImageLoaded(),
      user: getUser(),
    };
  }

  componentDidMount() {
    document.addEventListener('keypress', (evt) => {
      switch (evt.code) {
        case 'KeyB':
          this.updateTool('PAINT_BUCKET');
          break;
        case 'KeyP':
          this.updateTool('PENCIL');
          break;
        case 'KeyC':
          this.updateTool('CHOOSE_COLOR');
          break;
        default:
      }
    });

    const imageDataPromise = getImageData();
    if (imageDataPromise instanceof Promise) {
      imageDataPromise.then((imageData) => this.setState({ imageData }));
    }
  }

  updatePixelSize(pixelSize) {
    setPixelSize(pixelSize);
    this.setState({ pixelSize });
  }

  onImageSrc(imageSrc) {
    drawImage(imageSrc).then((imageData) => {
      setImageLoaded(true);
      setImageData(imageData);
      this.setState({ imageData, imageLoaded: true });
    });
  }

  updateTool(tool) {
    setTool(tool);
    this.setState({ tool });
  }

  updateColors(color) {
    if (!colorsEqual(color, this.state.currentColor)) {
      setCurrentColor(color);
      setPreviousColor(this.state.currentColor);
      this.setState((state) => ({ currentColor: color, previousColor: state.currentColor }));
    }
  }

  pixel(x, y) {
    let { imageData } = this.state;
    const { currentColor, pixelSize } = this.state;
    imageData = drawPixel(imageData, currentColor, pixelSize, x, y);
    setImageData(imageData);
    this.setState({ imageData, pencilX: x, pencilY: y });
  }

  line(x1, y1) {
    const { pencilX: x0, pencilY: y0 } = this.state;
    if (x0 != null && y0 != null && (x0 !== x1 || y0 !== y1)) {
      let { imageData } = this.state;
      const { currentColor, pixelSize } = this.state;

      imageData = drawLine(imageData, currentColor, pixelSize, x0, y0, x1, y1);
      this.setState({
        imageData,
        pencilX: x1,
        pencilY: y1,
      });
    }
  }

  fill(originX, originY) {
    let { imageData } = this.state;
    const { currentColor, pixelSize } = this.state;
    imageData = floodFill(imageData, currentColor, pixelSize, originX, originY);
    setImageData(imageData);
    this.setState({ imageData });
  }

  bw() {
    this.setState((state) => {
      const imageData = toBW(state.imageData);
      setImageData(imageData);
      return { imageData };
    });
  }

  onMouseDown(x, y) {
    switch (this.state.tool) {
      case 'PENCIL':
        this.pixel(x, y);
        break;
      case 'PAINT_BUCKET':
        this.fill(x, y);
        break;
      case 'CHOOSE_COLOR':
        this.updateColors(getColorAt(this.state.imageData, x, y));
        break;
      default:
    }
  }

  onMouseMove(x, y) {
    switch (this.state.tool) {
      case 'PENCIL':
        this.line(x, y);
        break;
      default:
    }
  }

  onMouseUp() {
    switch (this.state.tool) {
      case 'PENCIL':
        setImageData(this.state.imageData);
        this.setState({ pencilX: null, pencilY: null });
        break;
      default:
    }
  }

  authenticate() {
    const authenticator = new netlify({});
    authenticator.authenticate({ provider: 'github', scope: 'user' }, (err, user) => {
      setUser(user);
      this.setState({ user });
    });
  }

  logOut() {
    removeUser();
    this.setState({ user: null });
  }

  render() {
    if (this.state.user !== null) {
      return (
        <div>
          <Header user={this.state.user} onLogOut={() => this.logOut()} />

          <main className="app">
            <Tools
              className="app__tools"
              tools={tools}
              selected={this.state.tool}
              onTool={(tool) => this.updateTool(tool)}
            />

            <Colors
              className="app__colors"
              colors={colors}
              current={this.state.currentColor}
              previous={this.state.previousColor}
              onColor={(color) => this.updateColors(color)}
            />

            <Canvas
              className="app__canvas"
              imageData={this.state.imageData}
              onMouseDown={(x, y) => this.onMouseDown(x, y)}
              onMouseUp={() => this.onMouseUp()}
              onMouseMove={(x, y) => this.onMouseMove(x, y)}
            />

            <RandomImage
              className="app__random-image"
              onImageSrc={(src) => this.onImageSrc(src)}
              onBW={() => this.bw()}
              imageLoaded={this.state.imageLoaded}
            />

            <Sizes
              className="app__sizes"
              sizes={pixelSizes}
              onSize={(pixelSize) => this.updatePixelSize(pixelSize)}
              current={this.state.pixelSize}
            />
          </main>
        </div>
      );
    }
    return (
      <button className="app__login" onclick={() => this.authenticate()}>
        Log in
      </button>
    );
  }
}
