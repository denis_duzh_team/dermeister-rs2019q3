class Node {
  constructor(data, priority) {
    this.data = data;
    this.priority = priority;

    this.left = null;
    this.right = null;
    this.parent = null;
  }

  appendChild(node) {
    if (!this.left || !this.right) {
      !this.left ? (this.left = node) : (this.right = node);
      node.parent = this;
    }
  }

  removeChild(node) {
    if (this.left === node || this.right === node) {
      this.left === node ? (this.left = null) : (this.right = null);
      node.parent = null;
    } else {
      throw new Error("Node has no such child");
    }
  }

  remove() {
    if (this.parent) this.parent.removeChild(this);
  }

  swapWithParent() {
    if (this.parent) {
      const thisLeft = this.left;
      const thisRight = this.right;

      const parent = this.parent;
      const grandParent = this.parent.parent;

      if (grandParent)
        grandParent.left === parent ? (grandParent.left = this) : (grandParent.right = this);
      this.parent = grandParent;

      if (parent.left === this) {
        this.left = parent;
        this.right = parent.right;
        if (this.right) this.right.parent = this;
      } else {
        this.right = parent;
        this.left = parent.left;
        if (this.left) this.left.parent = this;
      }
      parent.left = thisLeft;
      parent.right = thisRight;
      parent.parent = this;

      if (thisLeft) thisLeft.parent = parent;
      if (thisRight) thisRight.parent = parent;
    }
  }
}

module.exports = Node;
