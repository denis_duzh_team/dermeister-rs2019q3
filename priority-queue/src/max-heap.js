const Node = require("./node");

class MaxHeap {
  constructor() {
    this.root = null;
    this.parentNodes = [];
    this._size = 0;
  }

  push(data, priority) {
    const node = new Node(data, priority);
    this.insertNode(node);
    this.shiftNodeUp(node);
    this._size++;
  }

  pop() {
    if (this.root) {
      this._size--;
      const result = this.root.data;

      const detached = this.detachRoot();
      this.restoreRootFromLastInsertedNode(detached);
      this.shiftNodeDown(this.root);

      return result;
    }
  }

  detachRoot() {
    if (this.root) {
      this._detachedRoot = this.root;

      this._detachedRootIndex = this.parentNodes.indexOf(this.root);
      if (this._detachedRootIndex !== -1) this.parentNodes.splice(this._detachedRootIndex, 1);

      this.root = null;
    }

    return this._detachedRoot;
  }

  restoreRootFromLastInsertedNode(detached) {
    if (this.parentNodes.length) {
      const lastInserted = this.parentNodes[this.parentNodes.length - 1];

      this.parentNodes.pop();
      if (lastInserted.parent && lastInserted.parent.left !== lastInserted)
        this.parentNodes.unshift(lastInserted.parent);

      const rootIndex = this.parentNodes.indexOf(detached);
      if (rootIndex !== -1) this.parentNodes.splice(rootIndex, 1, lastInserted);

      lastInserted.left = detached.left;
      if (lastInserted.left) lastInserted.left.parent = lastInserted;

      lastInserted.right = detached.right;
      if (lastInserted.right) lastInserted.right.parent = lastInserted;

      lastInserted.remove();
      this.root = lastInserted;
    }
  }

  size() {
    return this._size;
  }

  isEmpty() {
    return !(this.root && this.parentNodes.length);
  }

  clear() {
    this.root = null;
    this.parentNodes = [];
    this._size = 0;
  }

  insertNode(node) {
    this.parentNodes.push(node);

    if (!this.root) {
      this.root = node;
    } else {
      const single = this.parentNodes[0];
      single.appendChild(node);

      if (single.left && single.right) this.parentNodes.shift();
    }
  }

  _updateParentNodes(node, newNode) {
    const nodeIndex = this.parentNodes.indexOf(node);
    const newNodeIndex = this.parentNodes.indexOf(newNode);

    if (nodeIndex !== -1) this.parentNodes.splice(nodeIndex, 1, newNode);
    if (newNodeIndex !== -1) this.parentNodes.splice(newNodeIndex, 1, node);
  }

  shiftNodeUp(node) {
    const parent = node.parent;

    if (parent && node.priority > parent.priority) {
      this._updateParentNodes(node, node.parent);
      node.swapWithParent();
      this.shiftNodeUp(node);
    } else {
      if (this.root.parent) this.root = node;
    }
  }

  shiftNodeDown(node, hasUpdated = false) {
    if (node && (node.left || node.right)) {
      const leftGreater = !node.right || node.left.priority > node.right.priority;

      if (node.priority < node.left.priority && leftGreater) {
        if (!hasUpdated && node === this.root) this.root = node.left;

        this._updateParentNodes(node, node.left);
        node.left.swapWithParent();

        this.shiftNodeDown(node, true);
      } else {
        if (node.right && node.priority < node.right.priority && !leftGreater) {
          if (!hasUpdated && node === this.root) this.root = node.right;

          this._updateParentNodes(node, node.right);
          node.right.swapWithParent();

          this.shiftNodeDown(node, true);
        }
      }
    }
  }
}

module.exports = MaxHeap;

const h = new MaxHeap();

h.push(42, 15);
h.restoreRootFromLastInsertedNode({});

// const root = h.root;
// const left = h.root.left;
// const lastInsertedNode = h.root.right;

// const detached = h.detachRoot();
// h.restoreRootFromLastInsertedNode(detached);

// expect(h.parentNodes.indexOf(root)).to.equal(-1);
// expect(h.parentNodes[0]).to.equal(lastInsertedNode);
// expect(h.parentNodes[1]).to.equal(left);
