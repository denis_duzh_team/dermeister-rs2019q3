# Denis Duzh

## Contacts

  **e-mail**: duzh.denis@gmail.com  
  **tel**: +375 (29) 502-17-68  
  **skype**: denis\_duzh  

## Summary

  Junior Front-End Developer aiming to stay in touch with the trends in the field and involve them into real projects.  

  Fond of learning and exploring new technologies, being convinced that deeper understanding of ones allows to create more valuable, impressive and complex applications. Able to learn incredibly fast due to genuine passion and dedication to the subject of learning.  

  Prefer working in a team considering a wonderful possibility to collaborate and join forces in achieving bigger goals.

## Skills

### Technologies
  - JS, HTML/CSS, Node.js, Typescript
  - BEM
  - React, Electron, Angular

### Tools
  - SCSS, Less, PostCSS, styled-components
  - Gulp, Webpack
  - Git, BitBucket, Github
  - VSCode, WebStorm, IntelliJ IDEA

### Other
  - Java, C/C++, Assembler(FASM), Delphi
  - MongoDB, PostgreSQL

## Code examples

### Javascript
```javascript
import { curry, pipe, filter, join } from "lodash/fp";
import capMap from "lodash/fp/map";
const map = capMap.convert({ cap: false });
 
const russian = "абвгдеёжзийклмнопрстуфхцчшщъыьэюя";
 
const lowerCase = letter => letter.toLowerCase();
const alphabetOnly = alphabet => filter(letter => alphabet.includes(lowerCase(letter)));
const russianAlphabetOnly = alphabetOnly(russian);
 
const normalizeVigenereLetters = pipe(
  russianAlphabetOnly,
  map(lowerCase)
);
 
const normalizeVigenereKey = pipe(
  normalizeVigenereLetters,
  join("")
);
 
const transformLetter = curry((alphabet, key, letter, index) => {
  const letterIndex = alphabet.indexOf(letter);
  const keyIndex = alphabet.indexOf(key[index % key.length]) + Math.floor(index / key.length);
  const transformLetterIndex = (letterIndex + keyIndex) % alphabet.length;
  return alphabet[transformLetterIndex];
});
const transformRussianLetter = transformLetter(russian);
const transformRussianLetters = key => map(transformRussianLetter(key));
 
export const vigenereCipher = (message, key) =>
  pipe(
    normalizeVigenereLetters,
    transformRussianLetters(normalizeVigenereKey(key)),
    join("")
  )(message);
```

### SCSS
```scss
.reviews {
  background-color: $white;

  @media (min-width: $tablet-width) {
    &::after {
      content: "";

      position: relative;
      bottom: 30px;

      display: block;
      height: 25px;

      background: url("img/bg-zigzag-line.svg") repeat-x;
    }
  }
}
```

## Experience

  - As a part of Lab work in Belarusian State University of Informatics and Radioelectronics, developed application for encrypting text using React and Electron
  - Created layouts for [Gllacy Shop](https://github.com/dermeister/gllacy) and [Mishka](https://github.com/dermeister/mishka) using PSD templates from HTMLAcademy
  - Take part in solving tasks on [codewars.com](https://www.codewars.com/users/dermeister), having achieved 4th kyu by now.

## Education

  - Belarusian State University of Informatics and Radioelectronics, Faculty of Computer Networks and Systems, Chair of Software Development, 2018-2021

## Courses

  - [HTMLAcademy Interactive Courses](https://htmlacademy.ru/profile/id1128067)
  - Belhard Academy, "Java Programming", 2016
  - Belhard Academy, "Java Web-development", 2016

## English

  I was practicing english language extensively, attending courses in [International House](https://www.ih.by/) and [Streamline](https://str.by/) from 2016 to 2018 and eventually reaching C1(Advanced) level with a corresponding certificate from Streamline school. After that I proceeded learning