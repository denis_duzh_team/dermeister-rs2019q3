import RS from './components/RS';
import App from './components/App/App';

import './style.scss';

RS.render(<App />, document.body);
