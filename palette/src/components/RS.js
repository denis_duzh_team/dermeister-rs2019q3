class RSElement {
  constructor(type, props) {
    this.type = type;
    this.props = props;
  }
}

const isElement = (obj) => obj instanceof RSElement;

const createElement = (type, props, ...children) => {
  props = props || {};

  props.children = children
    .flat(Infinity)
    .map((child) => (isElement(child) ? child : createElement('text', { nodeValue: child })));

  return new RSElement(type, props);
};

const createDOMElement = (type) => {
  if (type === 'text') return document.createTextNode('');
  return document.createElement(type);
};

const render = (element, container) => reconcile(element, null, container);

const reconcile = (element, instance, container) => {
  if (!instance) return reconcileNewInstance(element, container);

  if (elementsEqual(element, instance.element)) {
    return isHTMLElement(element.type)
      ? reconcileUpdateHTMLElement(element, instance)
      : reconcileUpdateComponent(element, instance, container);
  }

  return reconcileReplaceInstance(element, instance, container);
};

const reconcileNewInstance = (element, container) => {
  const instance = instantiate(element);
  container.appendChild(instance.dom);
  invokeDidMount(instance);

  return instance;
};

const reconcileUpdateHTMLElement = (element, instance) => {
  setAttributes(instance.dom, element.props, instance.element.props);

  instance.childInstances = reconcileUpdateChildren(element, instance);
  instance.element = element;

  return instance;
};

const reconcileUpdateComponent = (element, instance, container) => {
  instance.publicInstance.props = element.props;
  instance.childInstance = reconcile(
    instance.publicInstance.render(),
    instance.childInstance,
    container,
  );
  instance.dom = instance.childInstance.dom;
  invokeDidUpdate(instance.publicInstance);

  return instance;
};

const reconcileReplaceInstance = (element, instance, container) => {
  const newInstance = instantiate(element);
  container.replaceChild(newInstance.dom, instance.dom);
  invokeDidMount(newInstance);

  return newInstance;
};

const cleanChildren = (children, instance) => {
  const childInstancesLength = instance.childInstances.length;

  if (children.length < childInstancesLength) {
    for (let i = children.length; i < childInstancesLength; i += 1) {
      instance.dom.removeChild(instance.childInstances[i].dom);
    }
  }
};

const reconcileUpdateChildren = (element, instance) => {
  const childInstances = [];

  cleanChildren(element.props.children, instance);

  for (let i = 0; i < element.props.children.length; i += 1) {
    const childElement = element.props.children[i];
    let childInstance = instance.childInstances[i];

    childInstance = reconcile(childElement, childInstance, instance.dom);
    childInstances.push(childInstance);
  }

  return childInstances;
};

const elementsEqual = (newElement, oldElement) => newElement.type === oldElement.type;

const isHTMLElement = (type) => typeof type === 'string';

const instantiate = (element) => {
  if (typeof element.type === 'string') {
    return instantiateHTMLElement(element);
  }
  return instantiateComponent(element);
};

const instantiateHTMLElement = (element) => {
  const { type, props } = element;

  const dom = createDOMElement(type);
  if (props.ref) props.ref.current = dom;
  setAttributes(dom, props);

  return {
    element,
    dom,
    childInstances: props.children.map((child) => reconcile(child, null, dom)),
  };
};

const instantiateComponent = (element) => {
  const { type, props } = element;

  const instance = {};
  const publicInstance = new type(props);
  publicInstance.__instance = instance;
  if (props.ref) props.ref.current = publicInstance;
  const childInstance = instantiate(publicInstance.render());

  return Object.assign(instance, {
    element,
    publicInstance,
    childInstance,
    dom: childInstance.dom,
  });
};

const setAttributes = (dom, nextProps, prevProps = {}) => {
  Object.keys(prevProps)
    .filter((key) => key !== 'children' && key !== 'ref')
    .forEach((key) => {
      if (dom[key] != nextProps[key]) {
        dom[key] = nextProps[key] || null;
      }
    });

  Object.keys(nextProps)
    .filter((key) => key !== 'children' && key !== 'ref')
    .forEach((key) => {
      if (dom[key] != nextProps[key]) dom[key] = nextProps[key];
    });
};

const reconcileAfterSetState = (instance) =>
  reconcile(instance.element, instance, instance.dom.parentNode);

const invokeDidMount = (instance) => {
  if (!isHTMLElement(instance.element.type) && instance.publicInstance.componentDidMount) {
    instance.publicInstance.componentDidMount();
  }
};

const invokeDidUpdate = (publicInstance) => {
  if (publicInstance.componentDidUpdate) publicInstance.componentDidUpdate();
};

const createRef = () => ({ current: null });

class Component {
  constructor(props) {
    this.props = props;
    this.state = {};
  }

  setState(state) {
    if (typeof state === 'function') this.state = { ...this.state, ...state(this.state) };
    else this.state = { ...this.state, ...state };
    reconcileAfterSetState(this.__instance);
  }
}

export default {
  Component,
  render,
  createElement,
  createRef,
};
