import RS from '../RS';

import Header from '../Header/Header';
import Tools from '../Tools/Tools';
import Colors from '../Colors/Colors';
import Canvas from '../Canvas/Canvas';

import './App.scss';
import initialImage from './image.json';

const tools = [
  { toolId: 'PAINT_BUCKET', title: 'Paint bucket', className: 'paint-bucket' },
  { toolId: 'CHOOSE_COLOR', title: 'Choose color', className: 'choose-color' },
  { toolId: 'PENCIL', title: 'Pencil', className: 'pencil' },
];

const colors = [
  { value: '#f74141', title: 'Red', className: 'red' },
  { value: '#00bcd4', title: 'Blue', className: 'blue' },
];

export default class App extends RS.Component {
  constructor(props) {
    super(props);

    this.state = {
      tool: localStorage.getItem('tool') || 'PENCIL',
      color: localStorage.getItem('current') || '#00ee00',
      previousColor: localStorage.getItem('previous') || '#00ee00',
      image: JSON.parse(localStorage.getItem('image')) || initialImage,
    };
  }

  componentDidMount() {
    document.addEventListener('keypress', (evt) => {
      switch (evt.code) {
        case 'KeyB':
          this.updateTool('PAINT_BUCKET');
          break;
        case 'KeyP':
          this.updateTool('PENCIL');
          break;
        case 'KeyC':
          this.updateTool('CHOOSE_COLOR');
          break;
        default:
      }
    });
  }

  updateTool(tool) {
    localStorage.setItem('tool', tool);
    this.setState({ tool });
  }

  updateColors(color) {
    if (color !== this.state.color) {
      localStorage.setItem('current', color);
      localStorage.setItem('previous', this.state.color);
      this.setState((state) => ({ color, previousColor: state.color }));
    }
  }

  calculateLineFunction(f, y, x, dy, dx, signY, signX) {
    f += dy;
    if (f > 0) {
      f -= dx;
      y += signY;
    }
    x -= signX;
    return [f, y, x];
  }

  drawPixel(x, y) {
    const { image } = this.state;

    image[y][x] = this.state.color;
    this.setState({ image, pencilX: x, pencilY: y });
  }

  drawLine(x1, y1) {
    const { pencilX: x0, pencilY: y0 } = this.state;
    const { image } = this.state;

    if (x0 != null && y0 != null && (x0 !== x1 || y0 !== y1)) {
      const signY = y1 - y0 < 0 ? -1 : 1;
      const signX = x0 - x1 < 0 ? -1 : 1;

      const dy = (y1 - y0) * signY;
      const dx = (x0 - x1) * signX;

      let f = 0;
      let x = x0;
      let y = y0;
      do {
        if (Math.abs(y1 - y0) <= Math.abs(x0 - x1)) {
          [f, y, x] = this.calculateLineFunction(f, y, x, dy, dx, signY, signX);
        } else {
          [f, x, y] = this.calculateLineFunction(f, x, y, dx, dy, -1 * signX, -1 * signY);
        }
        image[y][x] = this.state.color;
      } while (x != x1 || y != y1);

      this.setState({ image, pencilX: x1, pencilY: y1 });
    }
  }

  floodFill(originX, originY) {
    const { image } = this.state;
    const oldColor = image[originY][originX];
    const stack = [{ x: originX, y: originY }];

    if (oldColor !== this.state.color) {
      while (stack.length > 0) {
        const { x, y } = stack.pop();
        image[y][x] = this.state.color;

        if (image[y + 1] && image[y + 1][x] === oldColor) stack.push({ x, y: y + 1 });
        if (image[y - 1] && image[y - 1][x] === oldColor) stack.push({ x, y: y - 1 });
        if (image[y] && image[y][x + 1] === oldColor) stack.push({ x: x + 1, y });
        if (image[y] && image[y][x - 1] === oldColor) stack.push({ x: x - 1, y });
      }
    }

    this.setState({ image });
  }

  onMouseDown(x, y) {
    switch (this.state.tool) {
      case 'PENCIL':
        this.drawPixel(x, y);
        break;
      case 'PAINT_BUCKET':
        this.floodFill(x, y);
        break;
      case 'CHOOSE_COLOR':
        this.updateColors(this.state.image[y][x]);
        break;
      default:
    }
  }

  onMouseMove(x, y) {
    switch (this.state.tool) {
      case 'PENCIL':
        this.drawLine(x, y);
        break;
      default:
    }
  }

  onMouseUp() {
    switch (this.state.tool) {
      case 'PENCIL':
        localStorage.setItem('image', JSON.stringify(this.state.image));
        this.setState({ pencilX: null, pencilY: null });
        break;
      default:
    }
  }

  render() {
    return (
      <div>
        <Header />

        <main className="app">
          <Tools
            className="app__tools"
            tools={tools}
            selected={this.state.tool}
            onTool={(tool) => this.setState({ tool })}
          />

          <Colors
            className="app__colors"
            colors={colors}
            current={this.state.color}
            previous={this.state.previousColor}
            onColor={(color) => this.updateColors(color)}
          />

          <Canvas
            className="app__canvas"
            scale={32}
            image={this.state.image}
            onMouseDown={(x, y) => this.onMouseDown(x, y)}
            onMouseUp={() => this.onMouseUp()}
            onMouseMove={(x, y) => this.onMouseMove(x, y)}
          />
        </main>
      </div>
    );
  }
}
