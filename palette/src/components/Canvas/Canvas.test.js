import Canvas from './Canvas';

describe('scalePixel()', () => {
  let canvas;
  it('Should scale pixel', () => {
    canvas = new Canvas({ scale: 128 });
    expect(canvas.scalePixel(120)).toBe(0);
    expect(canvas.scalePixel(140)).toBe(1);
  });
});
