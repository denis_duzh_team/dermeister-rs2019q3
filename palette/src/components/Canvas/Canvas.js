import RS from '../RS';

import './Canvas.scss';

export default class Canvas extends RS.Component {
  constructor(props) {
    super(props);

    this.canvas = RS.createRef();
  }

  componentDidMount() {
    this.ctx = this.canvas.current.getContext('2d');
    this.drawImage();
  }

  componentDidUpdate() {
    this.drawImage();
  }

  scalePixel(pixel) {
    const { scale } = this.props;
    return Math.floor(pixel / scale);
  }

  mouseDown({ offsetX: x, offsetY: y }) {
    this.props.onMouseDown(this.scalePixel(x), this.scalePixel(y));
  }

  mouseUp() {
    this.props.onMouseUp();
  }

  mouseMove({ offsetX: x, offsetY: y }) {
    this.props.onMouseMove(this.scalePixel(x), this.scalePixel(y));
  }

  drawPixel(x, y, color) {
    const { scale } = this.props;

    this.ctx.fillStyle = color;
    this.ctx.fillRect(x * scale, y * scale, scale, scale);
  }

  drawImage() {
    const { image } = this.props;

    for (let i = 0; i < image.length; i += 1) {
      for (let j = 0; j < image[i].length; j += 1) {
        this.drawPixel(j, i, image[i][j]);
      }
    }
  }

  render() {
    return (
      <canvas
        ref={this.canvas}
        className={`canvas ${this.props.className}`}
        width="512"
        height="512"
        onmousedown={(e) => this.mouseDown(e)}
        onmouseup={() => this.mouseUp()}
        onmousemove={(e) => this.mouseMove(e)}
      >
        Your browser doesn't support canvas
      </canvas>
    );
  }
}
