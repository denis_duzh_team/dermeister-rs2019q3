import RS from '../RS';

import RadioInput from '../RadioInput/RadioInput';

import './Tools.scss';

export default class Tools extends RS.Component {
  isToolSelected({ toolId }) {
    return this.props.selected === toolId;
  }

  itemClassName(tool) {
    return `tools__item${this.isToolSelected(tool) ? ' tools__item--selected' : ''}`;
  }

  toolClassName(tool) {
    return `tools__btn tools__btn--${tool.className}`;
  }

  render() {
    return (
      <ul className={`tools ${this.props.className}`}>
        {this.props.tools.map((tool) => (
          <li className={this.itemClassName(tool)}>
            <RadioInput
              className={this.toolClassName(tool)}
              value={tool.toolId}
              label={tool.title}
              id={tool.toolId}
              name="tools"
              checked={this.isToolSelected(tool)}
              onchange={this.props.onTool}
            />
          </li>
        ))}
      </ul>
    );
  }
}
