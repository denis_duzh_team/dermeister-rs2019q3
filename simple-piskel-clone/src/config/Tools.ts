import { ToolType } from "../types/types";

export const tools = [
  {
    type: ToolType.PEN,
    offsetX: -184,
    offsetY: -93
  },
  {
    type: ToolType.COLOR_SELECT,
    offsetX: -275,
    offsetY: 0
  },
  {
    type: ToolType.PAINT_BUCKET,
    offsetX: -230,
    offsetY: -93
  },
  {
    type: ToolType.ERASER,
    offsetX: -47,
    offsetY: -230
  },
  {
    type: ToolType.ALL_PIXELS_SAME_COLOR,
    offsetX: -230,
    offsetY: -141
  },
  {
    type: ToolType.STROKE,
    offsetX: -49,
    offsetY: -137
  }
];
