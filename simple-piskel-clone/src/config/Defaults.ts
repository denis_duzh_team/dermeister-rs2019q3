import { ToolType, FrameShortcutActionType, ExportType } from "../types/types";

export default {
  primaryColor: { r: 0, g: 0, b: 0, a: 255 },
  secondaryColor: { r: 255, g: 255, b: 255, a: 0 },
  paintAreaSize: 16,
  paintAreaActualSize: 512,
  frameSize: 96,
  previewSize: 200,
  fps: 1,
  lowFps: 1,
  highFps: 24,
  oauthId: "798514167927-svi0fsid4fdt37dajqt4nsiuakepmi9b.apps.googleusercontent.com",
  shortcutSections: [
    {
      sectionName: "tools",
      shortcuts: [
        {
          value: ToolType.PEN,
          keys: "KeyP",
          description: "Pen tool"
        },
        {
          value: ToolType.PAINT_BUCKET,
          keys: "KeyB",
          description: "Paint bucket tool"
        },
        {
          value: ToolType.ERASER,
          keys: "KeyE",
          description: "Pen tool"
        },
        {
          value: ToolType.ALL_PIXELS_SAME_COLOR,
          keys: "KeyA",
          description: "Magic bucket tool"
        },
        {
          value: ToolType.STROKE,
          keys: "KeyL",
          description: "Stroke tool"
        },
        {
          value: ToolType.COLOR_SELECT,
          keys: "KeyO",
          description: "Color select"
        }
      ]
    },
    {
      sectionName: "frames",
      shortcuts: [
        {
          value: FrameShortcutActionType.CREATE,
          keys: "KeyN",
          description: "Create new frame"
        },
        {
          value: FrameShortcutActionType.DUPLICATE,
          keys: "KeyM",
          description: "Duplicate frame"
        },
        {
          value: FrameShortcutActionType.DELETE,
          keys: "KeyD",
          description: "Delete frame"
        }
      ]
    },
    {
      sectionName: "export",
      shortcuts: [
        {
          value: ExportType.GIF,
          keys: "KeyI",
          description: "Export as GIF"
        },
        {
          value: ExportType.APNG,
          keys: "KeyY",
          description: "Export as APNG"
        }
      ]
    }
  ]
};
