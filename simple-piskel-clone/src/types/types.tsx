export type Frame = Uint8ClampedArray;
export type NullableHTMLCanvasElement = HTMLCanvasElement | null;
export type ChangeEvent = React.ChangeEvent<HTMLInputElement>;

export enum ToolType {
  PEN = "PEN",
  COLOR_SELECT = "COLOR_SELECT",
  PAINT_BUCKET = "PAINT_BUCKET",
  ERASER = "ERASER",
  ALL_PIXELS_SAME_COLOR = "ALL_PIXELS_SAME_COLOR",
  STROKE = "STROKE"
}

export enum PaintActionType {
  NEW_FRAME,
  NEW_COLOR
}

export enum FrameShortcutActionType {
  CREATE,
  DELETE,
  DUPLICATE
}

export enum ExportType {
  GIF,
  APNG
}

export interface Tool {
  type: ToolType;
  offsetX: number;
  offsetY: number;
}

export interface RGBAColor {
  r: number;
  g: number;
  b: number;
  a: number;
}

export interface Point {
  x: number;
  y: number;
}

export interface PaintConfig {
  penSize: number;
  paintAreaSize: number;
  color: RGBAColor;
}

export interface PaintAction {
  type: PaintActionType;
  value: Frame | RGBAColor;
}

export interface FrameAction {
  frames?: Frame[];
  current: number;
}

export interface PaintFactory {
  line: (to: Point) => Frame;
  color: () => RGBAColor;
  fill: () => Frame;
  erase: (to: Point) => Frame;
  sameColor: () => Frame;
  stroke: (to: Point) => Frame;
}

export interface Shortcut {
  value?: ToolType | FrameShortcutActionType | ExportType;
  keys: string;
  description: string;
}

export interface ShortcutSection {
  sectionName: string;
  shortcuts: Shortcut[];
}
