import { curry } from "lodash/fp";

import { Tool, RGBAColor, ToolType } from "../types/types";

export const getToolByType = curry(
  (tools: Tool[], type: ToolType): Tool => tools.find(tool => type === tool.type) as Tool
);

export const hexToRGBA = (hex: string): RGBAColor => {
  let color = { r: 0, g: 0, b: 0, a: 0 };
  color.r = parseInt(hex.slice(1, 3), 16);
  color.g = parseInt(hex.slice(3, 5), 16);
  color.b = parseInt(hex.slice(5, 7), 16);
  color.a = 255;

  return color;
};

export const RGBAToHex = ({ r, g, b }: RGBAColor): string => {
  const rHex = r.toString(16).padStart(2, "0");
  const gHex = g.toString(16).padStart(2, "0");
  const bHex = b.toString(16).padStart(2, "0");

  return `#${rHex}${gHex}${bHex}`;
};
