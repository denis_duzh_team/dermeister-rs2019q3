import { Frame, RGBAColor, Point, PaintConfig, PaintFactory } from "../types/types";

export const initFrame = (width: number, heigth: number) =>
  new Uint8ClampedArray(4 * width * heigth);

export const resizeFrames = (frames: Frame[], newSize: number) =>
  frames.map(() => initFrame(newSize, newSize));

const colorsEqual = (color1: RGBAColor | null, color2: RGBAColor | null) => {
  if (!color1 || !color2) return false;

  const { r: r1, g: g1, b: b1, a: a1 } = color1;
  const { r: r2, g: g2, b: b2, a: a2 } = color2;

  return r1 === r2 && g1 === g2 && b1 === b2 && a1 === a2;
};

const validatePoint = ({ x, y }: Point, paintAreaSize: number) =>
  x >= 0 && x < paintAreaSize && y >= 0 && y < paintAreaSize;

const _color = (frame: Frame, point: Point, { paintAreaSize }: PaintConfig) => {
  const { x, y } = point;

  const r = frame[paintAreaSize * 4 * y + x * 4];
  const g = frame[paintAreaSize * 4 * y + x * 4 + 1];
  const b = frame[paintAreaSize * 4 * y + x * 4 + 2];
  const a = frame[paintAreaSize * 4 * y + x * 4 + 3];

  return { r, g, b, a };
};

const _pixel = (frame: Frame, at: Point, config: PaintConfig) => {
  const { x, y } = at;
  const { paintAreaSize, penSize, color } = config;
  const { r, g, b, a } = color;

  for (let i = 0; i < penSize; i += 1) {
    for (let j = 0; j < penSize; j += 1) {
      if (validatePoint({ x: x + j, y: y + i }, paintAreaSize)) {
        frame[paintAreaSize * 4 * (y + i) + (x + j) * 4] = r;
        frame[paintAreaSize * 4 * (y + i) + (x + j) * 4 + 1] = g;
        frame[paintAreaSize * 4 * (y + i) + (x + j) * 4 + 2] = b;
        frame[paintAreaSize * 4 * (y + i) + (x + j) * 4 + 3] = a;
      }
    }
  }

  return frame;
};

const calculateLineFunction = (
  f: number,
  y: number,
  x: number,
  dy: number,
  dx: number,
  signY: number,
  signX: number
) => {
  f += dy;
  if (f > 0) {
    f -= dx;
    y += signY;
  }
  x -= signX;
  return [f, y, x];
};

const _line = (frame: Frame, from: Point, to: Point, config: PaintConfig) => {
  const { x: x0, y: y0 } = from;
  const { x: x1, y: y1 } = to;
  if (x0 === x1 && y0 === y1) return _pixel(frame, from, config);

  const signY = y1 - y0 < 0 ? -1 : 1;
  const signX = x0 - x1 < 0 ? -1 : 1;

  const dy = (y1 - y0) * signY;
  const dx = (x0 - x1) * signX;

  let f = 0;
  let x = x0;
  let y = y0;

  do {
    if (Math.abs(y1 - y0) <= Math.abs(x0 - x1)) {
      [f, y, x] = calculateLineFunction(f, y, x, dy, dx, signY, signX);
    } else {
      [f, x, y] = calculateLineFunction(f, x, y, dx, dy, -1 * signX, -1 * signY);
    }
    frame = _pixel(frame, { x, y }, config);
  } while (x !== x1 || y !== y1);

  return frame;
};

const _fill = (frame: Frame, from: Point, config: PaintConfig) => {
  const oldColor = _color(frame, from, config);
  const stack: Point[] = [from];

  if (!colorsEqual(oldColor, config.color)) {
    while (stack.length > 0) {
      const { x, y } = stack.pop() as Point;
      frame = _pixel(frame, { x, y }, { ...config, penSize: 1 });

      let next = { x, y: y + 1 };
      if (
        validatePoint(next, config.paintAreaSize) &&
        colorsEqual(_color(frame, { x, y: y + 1 }, config), oldColor)
      )
        stack.push({ x, y: y + 1 });

      next = { x, y: y - 1 };
      if (
        validatePoint(next, config.paintAreaSize) &&
        colorsEqual(_color(frame, { x, y: y - 1 }, config), oldColor)
      )
        stack.push({ x, y: y - 1 });

      next = { x: x + 1, y };
      if (
        validatePoint(next, config.paintAreaSize) &&
        colorsEqual(_color(frame, { x: x + 1, y }, config), oldColor)
      )
        stack.push({ x: x + 1, y });

      next = { x: x - 1, y };
      if (
        validatePoint(next, config.paintAreaSize) &&
        colorsEqual(_color(frame, { x: x - 1, y }, config), oldColor)
      )
        stack.push({ x: x - 1, y });
    }
  }

  return frame;
};

const _sameColor = (frame: Frame, from: Point, config: PaintConfig) => {
  const { paintAreaSize } = config;
  const oldColor = _color(frame, from, config);

  for (let i = 0; i < paintAreaSize; i++) {
    for (let j = 0; j < paintAreaSize; j++) {
      const point = { x: i, y: j };
      const pixelColor = _color(frame, point, config);
      if (colorsEqual(oldColor, pixelColor)) {
        frame = _pixel(frame, point, config);
      }
    }
  }

  return frame;
};

export const createPaintFactory = (frame: Frame, at: Point, config: PaintConfig): PaintFactory => {
  const eraseConfig = { ...config, color: { ...config.color, a: 0 } };
  let buffer = frame;

  return {
    line(to: Point) {
      const from = at;
      const _frame = _line(frame, from, to, config);
      from.x = to.x;
      from.y = to.y;
      return _frame;
    },
    color() {
      return _color(frame, at, config);
    },
    fill() {
      return _fill(frame, at, config);
    },
    erase(to: Point) {
      const from = at;
      const _frame = _line(frame, from, to, eraseConfig);
      from.x = to.x;
      from.y = to.y;
      return _frame;
    },
    sameColor() {
      return _sameColor(frame, at, config);
    },
    stroke(to: Point) {
      frame = buffer.slice(0);
      return _line(frame, at, to, config);
    }
  };
};
