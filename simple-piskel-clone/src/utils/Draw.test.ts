import { initFrame, resizeFrames, createPaintFactory } from "./Draw";
import { PaintFactory, RGBAColor } from "../types/types";

describe("initFrame()", () => {
  it("should init frame", () => {
    let width = 2;
    let height = 2;
    let frame = Uint8ClampedArray.from(Array(width * height * 4).fill(0));
    expect(initFrame(width, height)).toEqual(frame);
  });
});

describe("resizeFrame()", () => {
  it("should resize frame", () => {
    let width = 2;
    let height = 2;
    let startFrame = Uint8ClampedArray.from(Array(width * height * 4).fill(0));
    width = 4;
    height = 4;
    let newFrame = Uint8ClampedArray.from(Array(width * height * 4).fill(0));
    expect(resizeFrames([startFrame], width)).toEqual([newFrame]);
  });
});

describe("createPaintFactory()", () => {
  let paintFactory: PaintFactory;

  beforeEach(() => {
    let color = { r: 255, g: 255, b: 255, a: 255 };
    let penSize = 1;
    let paintAreaSize = 2;
    let frame = Uint8ClampedArray.from(Array(paintAreaSize * paintAreaSize * 4).fill(0));
    paintFactory = createPaintFactory(frame, { x: 0, y: 0 }, { color, penSize, paintAreaSize });
  });

  it("should create paint factory", () => {
    expect(paintFactory).toBeDefined();
  });

  it("should draw line", () => {
    let expectedArray = [0, 0, 0, 0, 0, 0, 0, 0, 255, 255, 255, 255, 0, 0, 0, 0];
    let expectedFrame = Uint8ClampedArray.from(expectedArray);
    expect(paintFactory.line({ x: 0, y: 1 })).toEqual(expectedFrame);
  });

  it("should draw line in opposite direction", () => {
    let expectedArray = [255, 255, 255, 255, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
    let expectedFrame = Uint8ClampedArray.from(expectedArray);
    let color = { r: 255, g: 255, b: 255, a: 255 };
    let penSize = 1;
    let paintAreaSize = 2;
    let frame = Uint8ClampedArray.from(Array(paintAreaSize * paintAreaSize * 4).fill(0));
    paintFactory = createPaintFactory(frame, { x: 1, y: 1 }, { color, penSize, paintAreaSize });
    expect(paintFactory.line({ x: 0, y: 0 })).toEqual(expectedFrame);

    expectedArray = [0, 0, 0, 0, 255, 255, 255, 255, 0, 0, 0, 0, 0, 0, 0, 0];
    expectedFrame = Uint8ClampedArray.from(expectedArray);
    frame = Uint8ClampedArray.from(Array(paintAreaSize * paintAreaSize * 4).fill(0));
    paintFactory = createPaintFactory(frame, { x: 0, y: 1 }, { color, penSize, paintAreaSize });
    expect(paintFactory.line({ x: 1, y: 0 })).toEqual(expectedFrame);
  });

  it("should work on ivalid point", () => {
    let expectedArray = [0, 0, 0, 0, 0, 0, 0, 0, 255, 255, 255, 255, 0, 0, 0, 0];
    let expectedFrame = Uint8ClampedArray.from(expectedArray);
    expect(paintFactory.line({ x: 0, y: 2 })).toEqual(expectedFrame);
  });

  it("should draw as pixel", () => {
    let expectedArray = [255, 255, 255, 255, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
    let expectedFrame = Uint8ClampedArray.from(expectedArray);
    expect(paintFactory.line({ x: 0, y: 0 })).toEqual(expectedFrame);
  });

  it("should pick color", () => {
    let expectedColor = { r: 0, g: 0, b: 0, a: 0 };
    expect(paintFactory.color()).toEqual(expectedColor);
  });

  it("should fill", () => {
    let expectedArray = [
      255,
      255,
      255,
      255,
      255,
      255,
      255,
      255,
      255,
      255,
      255,
      255,
      255,
      255,
      255,
      255
    ];
    let expectedFrame = Uint8ClampedArray.from(expectedArray);
    expect(paintFactory.fill()).toEqual(expectedFrame);
  });

  it("should fill upside", () => {
    let color = { r: 255, g: 255, b: 255, a: 255 };
    let penSize = 1;
    let paintAreaSize = 2;
    let frame = Uint8ClampedArray.from(Array(paintAreaSize * paintAreaSize * 4).fill(0));
    paintFactory = createPaintFactory(frame, { x: 1, y: 1 }, { color, penSize, paintAreaSize });
    let expectedArray = [
      255,
      255,
      255,
      255,
      255,
      255,
      255,
      255,
      255,
      255,
      255,
      255,
      255,
      255,
      255,
      255
    ];
    let expectedFrame = Uint8ClampedArray.from(expectedArray);
    expect(paintFactory.fill()).toEqual(expectedFrame);
  });

  it("should fill with same color", () => {
    let color = { r: 0, g: 0, b: 0, a: 0 };
    let penSize = 1;
    let paintAreaSize = 2;
    let frame = Uint8ClampedArray.from(Array(paintAreaSize * paintAreaSize * 4).fill(0));
    paintFactory = createPaintFactory(frame, { x: 1, y: 1 }, { color, penSize, paintAreaSize });
    let expectedArray = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
    let expectedFrame = Uint8ClampedArray.from(expectedArray);
    expect(paintFactory.fill()).toEqual(expectedFrame);
  });

  it("should erase", () => {
    let expectedArray = [0, 0, 0, 0, 0, 0, 0, 0, 255, 255, 255, 0, 0, 0, 0, 0];
    let expectedFrame = Uint8ClampedArray.from(expectedArray);
    expect(paintFactory.erase({ x: 0, y: 1 })).toEqual(expectedFrame);
  });

  it("should fill same color", () => {
    let expectedArray = [
      255,
      255,
      255,
      255,
      255,
      255,
      255,
      255,
      255,
      255,
      255,
      255,
      255,
      255,
      255,
      255
    ];
    let expectedFrame = Uint8ClampedArray.from(expectedArray);
    expect(paintFactory.sameColor()).toEqual(expectedFrame);
  });

  it("should paint stroke", () => {
    let expectedArray = [0, 0, 0, 0, 0, 0, 0, 0, 255, 255, 255, 255, 0, 0, 0, 0];
    let expectedFrame = Uint8ClampedArray.from(expectedArray);
    expect(paintFactory.stroke({ x: 0, y: 1 })).toEqual(expectedFrame);
  });
});
