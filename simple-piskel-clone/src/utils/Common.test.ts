import { tools } from "../config/Tools";
import { hexToRGBA, RGBAToHex, getToolByType } from "./Common";
import { ToolType } from "../types/types";

describe("hexToRGBA()", () => {
  it("should convert hex to rgba", () => {
    let hex = "#fefefe";
    let rgba = { r: 254, b: 254, g: 254, a: 255 };
    expect(hexToRGBA(hex)).toEqual(rgba);
  });

  it("should convert uppercase hex to rgba", () => {
    let hex = "#FEFEFE";
    let rgba = { r: 254, b: 254, g: 254, a: 255 };
    expect(hexToRGBA(hex)).toEqual(rgba);
  });
});

describe("RGBAToHex()", () => {
  it("should convert rgba to hex", () => {
    let rgba = { r: 254, b: 254, g: 254, a: 255 };
    let hex = "#fefefe";
    expect(RGBAToHex(rgba)).toEqual(hex);
  });

  it("should return invaild hex on invalid rgba", () => {
    let rgba = { r: 1000, b: 254, g: 254, a: 255 };
    let hex = "#3e8fefe";
    expect(RGBAToHex(rgba)).toEqual(hex);
  });
});

describe("getToolByType()", () => {
  it("should return tool by type", () => {
    let toolType = ToolType.ERASER;
    let tool = getToolByType(tools, toolType);
    expect(tool).toEqual({
      type: ToolType.ERASER,
      offsetX: -47,
      offsetY: -230
    });
  });

  it("should return undefined on invalid type", () => {
    let toolType = "Some type" as ToolType;
    let tool = getToolByType(tools, toolType);
    expect(tool).toBeUndefined();
  });
});
