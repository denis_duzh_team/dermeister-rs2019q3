import React, { useState } from "react";
import styled from "styled-components";
import { pipe, get } from "lodash/fp";

interface PaintAreaSizeProps {
  className?: string;
  paintAreaSize: number;
  onPaintAreaSize: (paintAreaSize: number) => void;
}

const Form = styled.form`
  display: flex;
`;

const Input = styled.input`
  margin-right: 8px;
  padding: 3px 10px;
  width: 100%;

  text-align: right;
  color: white;
  font-size: 16px;

  border: 1px solid #888;
  background-color: black;
  border-radius: 2px;
  outline: none;

  &:focus {
    border-color: #ffd400;
  }
`;

const Resize = styled.button`
  color: black;
  text-align: center;
  font-size: 16px;

  background-color: gold;
  border: none;
  border-radius: 2px;
  cursor: pointer;
  outline: none;

  &:hover {
    background-color: white;
  }
`;

export default ({ onPaintAreaSize, paintAreaSize, className }: PaintAreaSizeProps) => {
  const [width, setWidth] = useState<string>(paintAreaSize.toString());

  const onSubmit = (e: React.FormEvent) => {
    e.preventDefault();
    pipe(Number, onPaintAreaSize)(width);
  };

  return (
    <Form className={className} onSubmit={onSubmit}>
      <Input
        id="width"
        value={width}
        placeholder="Size"
        onChange={pipe(get("target.value"), setWidth)}
      />
      <Resize>Resize</Resize>
    </Form>
  );
};
