import React from "react";
import { render } from "@testing-library/react";
import PaintAreaSize from "./PaintAreaSize";

test("renders PaintAreaSize", () => {
  const renderResult = render(<PaintAreaSize paintAreaSize={0} onPaintAreaSize={() => {}} />);

  expect(renderResult).toBeDefined();
});
