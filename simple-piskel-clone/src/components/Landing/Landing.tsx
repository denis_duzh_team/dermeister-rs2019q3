import React from "react";
import { Link } from "react-router-dom";
import styled from "styled-components";

import screenshotLarge from "../../img/screenshot-large.png";

const Navbar = styled.div`
  display: flex;
  align-items: center;
  justify-content: flex-end;

  padding-right: 10px;
  width: 100%;
  height: 50px;

  background-color: #2d2d2d;
`;

const NavbarLink = styled(Link)`
  display: block;
  padding: 2px 7px;

  color: black;
  text-align: center;
  font-size: 16px;
  text-decoration: none;

  background-color: gold;
  border: none;
  border-radius: 2px;
  cursor: pointer;
  outline: none;

  &:hover {
    background-color: white;
  }
`;

const Promo = styled.section`
  min-height: 600px;

  color: white;
  font-size: 36px;

  background: url(${screenshotLarge}) #505050 no-repeat 90% center;
  background-size: 1000px;
`;

const Description = styled.p`
  margin: 0;
  padding: 0;
  margin-bottom: 20px;
  padding-top: 20px;
  padding-left: 20px;

  font-weight: bold;

  width: 600px;
`;

const LinkLarge = styled(Link)`
  display: block;
  margin-left: 20px;
  padding: 10px 25px;
  width: 200px;

  color: black;
  text-align: center;
  font-size: 32px;
  text-decoration: none;

  background-color: gold;
  border: none;
  border-radius: 2px;
  cursor: pointer;
  outline: none;

  &:hover {
    background-color: white;
  }
`;

const Features = styled.section`
  color: white;

  background-color: #212121;
`;

const Header = styled.h2`
  margin: 0;
  padding: 20px;

  font-size: 32px;
`;

const FeaturesList = styled.ul`
  margin: 0;
  padding: 0;
  padding-left: 20px;
  padding-bottom: 20px;

  font-size: 25px;

  list-style: none;
`;

const Footer = styled.footer`
  display: flex;
  justify-content: center;

  background-color: #101010;
`;

const Github = styled.a`
  padding: 20px 0;

  color: white;
  text-decoration: white;
  font-size: 25px;
`;

export default () => (
  <div>
    <Navbar>
      <NavbarLink to="/app">Create sprite</NavbarLink>
    </Navbar>

    <main>
      <Promo>
        <Description>Piskel is a free online editor for animated sprites & pixel art</Description>
        <LinkLarge to="/app">App</LinkLarge>
      </Promo>

      <Features>
        <Header>Implemented features</Header>
        <FeaturesList>
          <li>Painting tools</li>
          <li>Fullscreen preview</li>
          <li>Frame management</li>
          <li>Shortcut configuration</li>
        </FeaturesList>
      </Features>

      <Footer>
        <Github href="https://github.com/dermeister">Denis Duzh</Github>
      </Footer>
    </main>
  </div>
);
