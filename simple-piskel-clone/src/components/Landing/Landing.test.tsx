import React from "react";
import { render } from "@testing-library/react";
import Landing from "./Landing";
import { Route, BrowserRouter as Router, Switch } from "react-router-dom";

test("renders Landing", () => {
  const renderResult = render(
    <Router>
      <Switch>
        <Route>
          <Landing />
        </Route>
      </Switch>
    </Router>
  );

  expect(renderResult).toBeDefined();
});
