import React, { useContext } from "react";
import styled from "styled-components";

import { ShortcutContext } from "../../context/context";
import { useShortcuts } from "../../hooks/hooks";
import { Frame, ShortcutSection, ExportType } from "../../types/types";

interface ExportProps {
  className?: string;
  frames: Frame[];
  fps: number;
  paintAreaSize: number;
}

const Export = styled.button`
  display: block;
  margin-bottom: 5px;
  padding: 5px 0;
  width: 100%;

  color: black;
  text-align: center;
  font-size: 16px;

  background-color: gold;
  border: none;
  border-radius: 2px;
  cursor: pointer;
  outline: none;

  &:hover {
    background-color: white;
  }
`;

export default ({ frames, className, fps, paintAreaSize }: ExportProps) => {
  const onExport = (type: ExportType) => {
    switch (type) {
      case ExportType.GIF:
        console.log("Gif");
        break;

      case ExportType.APNG:
        console.log("Apng");
        break;
    }
  };

  const section = useContext<ShortcutSection[]>(ShortcutContext).find(
    ({ sectionName }) => sectionName === "export"
  );

  useShortcuts(section, onExport);

  return (
    <div className={className}>
      <Export onClick={() => onExport(ExportType.GIF)}>GIF</Export>
      <Export onClick={() => onExport(ExportType.APNG)}>APNG</Export>
    </div>
  );
};
