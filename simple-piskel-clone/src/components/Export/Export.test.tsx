import React from "react";
import { render } from "@testing-library/react";
import Export from "./Export";

test("renders Export", () => {
  const renderResult = render(<Export frames={[]} fps={1} paintAreaSize={4} />);

  expect(renderResult.getByText("GIF")).toBeDefined();
  expect(renderResult.getByText("APNG")).toBeDefined();
});
