import React from "react";
import { render } from "@testing-library/react";
import Shortcuts from "./Shortcuts";

test("renders Shortcuts", () => {
  const renderResult = render(<Shortcuts onSections={() => {}} sections={[]} />);

  expect(renderResult).toBeDefined();
});
