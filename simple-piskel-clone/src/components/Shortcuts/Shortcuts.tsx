import React, { useState, useEffect } from "react";
import styled from "styled-components";
import { pipe, get } from "lodash/fp";

import { ShortcutSection } from "../../types/types";

import icons from "../../img/icons.png";

interface ShortcutsProps {
  className?: string;
  sections: ShortcutSection[];
  onSections: (sections: ShortcutSection[]) => void;
}

interface ShortcutsModalProps {
  visible: boolean;
}

const ShortcutsModal = styled.div`
  position: absolute;
  left: 50%;
  top: 50%;
  z-index: 100;

  display: ${({ visible }: ShortcutsModalProps) => (visible ? "block" : "none")};
  padding: 15px;
  width: 80vw;
  height: 80vh;

  background-color: black;
  border: 10px solid #ffd400;
  border-top-width: 40px;
  transform: translate(-50%, -50%);
`;

const ShortcutSectionList = styled.ul`
  display: flex;
  justify-content: space-between;

  margin: 0;
  padding: 0;

  list-style: none;
`;

const SectionName = styled.p`
  margin: 0;
  margin-bottom: 10px;
  padding: 0;

  font-size: 24px;
  font-weight: bold;
  text-transform: uppercase;
`;

const ShortcutList = styled.ul`
  margin: 0;
  padding: 0;

  list-style: none;
`;

const Input = styled.input`
  margin-right: 8px;
  padding: 3px 10px;
  width: 40px;

  color: white;
  font-size: 16px;

  border: 1px solid #888;
  background-color: black;
  border-radius: 2px;
  outline: none;

  &:focus {
    border-color: #ffd400;
  }
`;

const ShortcutItem = styled.li`
  margin-bottom: 10px;
`;

const ShowShortcuts = styled.button`
  width: 36px;
  height: 21px;

  background: url(${icons}) no-repeat -274px -136px;
  border: none;
  outline: none;
  cursor: pointer;
`;

export default ({ sections, onSections, className }: ShortcutsProps) => {
  const [modalVisible, setModalVisible] = useState<boolean>(false);

  useEffect(() => {
    const listener = (e: KeyboardEvent) => {
      if (modalVisible && e.code === "Escape") setModalVisible(false);
    };
    document.addEventListener("keydown", listener);

    return () => document.removeEventListener("keydown", listener);
  });

  const onChange = (secIndex: number, shorIndex: number) => (value: string) => {
    const current = `Key${value.toUpperCase()}`;
    const previous = sections[secIndex].shortcuts[shorIndex].keys;

    for (let i = 0; i < sections.length; i++) {
      for (let j = 0; j < sections[i].shortcuts.length; j++) {
        if (sections[i].shortcuts[j].keys === current) {
          sections[i].shortcuts[j].keys = previous;
        }
      }
    }

    sections[secIndex].shortcuts[shorIndex].keys = current;
    onSections(sections.slice(0));
  };

  return (
    <div className={className}>
      <ShowShortcuts onClick={() => setModalVisible(true)} />

      <ShortcutsModal visible={modalVisible}>
        <ShortcutSectionList>
          {sections.map((section, secIndex) => (
            <li key={secIndex}>
              <SectionName>{section.sectionName}</SectionName>
              <ShortcutList>
                {section.shortcuts.map((shortcut, shorIndex) => (
                  <ShortcutItem key={shorIndex}>
                    <Input
                      value={shortcut.keys[3] || ""}
                      onChange={pipe(get("target.value"), onChange(secIndex, shorIndex))}
                      onFocus={(e: React.FocusEvent<HTMLInputElement>) => e.target.select()}
                    />{" "}
                    {shortcut.description}
                  </ShortcutItem>
                ))}
              </ShortcutList>
            </li>
          ))}
        </ShortcutSectionList>
      </ShortcutsModal>
    </div>
  );
};
