import React, { useState } from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import styled from "styled-components";

import {
  Tool,
  RGBAColor,
  Frame,
  PaintAction,
  PaintActionType,
  FrameAction,
  ShortcutSection
} from "../../types/types";
import { penSizes } from "../../config/PenSizes";
import { tools } from "../../config/Tools";
import defaults from "../../config/Defaults";
import { initFrame, resizeFrames } from "../../utils/Draw";
import { ShortcutContext } from "../../context/context";

import Navbar from "../Navbar/Navbar";
import PenSizes from "../PenSizes/PenSizes";
import PaintTools from "../PaintTools/PaintTools";
import ColorPicker from "../ColorPicker/ColorPicker";
import PaintArea from "../PaintArea/PaintArea";
import Frames from "../Frames/Frames";
import Preview from "../Preview/Preview";
import PaintAreaSize from "../PaintAreaSize/PaintAreaSize";
import Export from "../Export/Export";
import Shortcuts from "../Shortcuts/Shortcuts";
import Landing from "../Landing/Landing";

const GlobalWrapper = styled.div`
  display: grid;
  grid-template-columns: 94px 110px auto 200px;
  grid-template-rows: repeat(3, auto) 1fr;
  grid-template-areas:
    "pen-sizes frames paint-area preview"
    "paint-tools frames paint-area paint-area-size"
    "color-picker frames paint-area export"
    "shortcuts frames paint-area .";
  grid-column-gap: 20px;
  align-items: flex-start;

  padding: 0 10px;
  height: 100vh;

  color: white;

  background-color: #1d1d1d;
`;

const StyledPenSizes = styled(PenSizes)`
  grid-area: pen-sizes;
  align-self: flex-end;
  justify-self: center;

  margin-bottom: 5px;
`;

const StyledPaintTools = styled(PaintTools)`
  grid-area: paint-tools;

  margin-bottom: 5px;
`;

const StyledColorPicker = styled(ColorPicker)`
  grid-area: color-picker;
`;

const StyledShortcuts = styled(Shortcuts)`
  grid-area: shortcuts;
  align-self: flex-end;

  margin-bottom: 10px;
`;

const StyledPaintArea = styled(PaintArea)`
  grid-area: paint-area;
  align-self: stretch;
`;

const StyledFrames = styled(Frames)`
  grid-area: frames;

  max-height: 600px;
  overflow: scroll;
`;

const StyledPreview = styled(Preview)`
  grid-area: preview;
`;

const StyledPaintAreaSize = styled(PaintAreaSize)`
  grid-area: paint-area-size;
  align-self: flex-end;
  margin-bottom: 5px;
`;

const StyledExport = styled(Export)`
  grid-area: export;
  margin-top: 10px;
`;

export default () => {
  const [penSize, setPenSize] = useState<number>(penSizes[0].value);
  const [tool, setTool] = useState<Tool>(tools[0]);
  const [primaryColor, setPrimaryColor] = useState<RGBAColor>(defaults.primaryColor);
  const [secondaryColor, setSecondaryColor] = useState<RGBAColor>(defaults.secondaryColor);
  const [paintAreaSize, setPaintAreaSize] = useState<number>(defaults.paintAreaSize);
  const [frames, setFrames] = useState<Frame[]>([
    initFrame(defaults.paintAreaSize, defaults.paintAreaSize)
  ]);
  const [currentFrame, setCurrentFrame] = useState<number>(0);
  const [shortcuts, setShortcuts] = useState<ShortcutSection[]>(defaults.shortcutSections);
  const [fps, setFps] = useState<number>(defaults.fps);
  const [userLogged, setUserLogged] = useState<boolean>(false);

  const onPaintAction = (action: PaintAction) => {
    switch (action.type) {
      case PaintActionType.NEW_FRAME:
        const newFrames = frames.slice(0);
        newFrames[currentFrame] = action.value as Frame;
        setFrames(newFrames);
        break;
      case PaintActionType.NEW_COLOR:
        const color = action.value as RGBAColor;
        setPrimaryColor(color);
        break;
    }
  };

  const onFrameAction = (action: FrameAction) => {
    setCurrentFrame(action.current as number);
    if (action.frames) setFrames(action.frames as Frame[]);
  };

  const onPaintAreaSize = (size: number) => {
    setPaintAreaSize(size);
    setFrames(resizeFrames(frames, size));
  };

  return (
    <Router>
      <Switch>
        <Route path="/app">
          <Navbar userLogged={userLogged} onUserLogged={setUserLogged} />
          <GlobalWrapper>
            <ShortcutContext.Provider value={shortcuts}>
              <StyledPenSizes penSize={penSize} onPenSize={setPenSize} />
              <StyledPaintTools current={tool} onTool={setTool} />
              <StyledColorPicker
                primary={primaryColor}
                onPrimary={setPrimaryColor}
                secondary={secondaryColor}
                onSecondary={setSecondaryColor}
              />
              <StyledPaintArea
                frame={frames[currentFrame]}
                tool={tool}
                config={{ paintAreaSize, penSize, color: primaryColor }}
                onAction={onPaintAction}
              />
              <StyledFrames
                frames={frames}
                currentFrame={currentFrame}
                paintAreaSize={paintAreaSize}
                onFrameAction={onFrameAction}
              />
              <StyledPreview frames={frames} size={paintAreaSize} fps={fps} onFps={setFps} />
              <StyledPaintAreaSize
                paintAreaSize={paintAreaSize}
                onPaintAreaSize={onPaintAreaSize}
              />
              <StyledExport frames={frames} fps={fps} paintAreaSize={paintAreaSize} />
              <StyledShortcuts sections={shortcuts} onSections={setShortcuts} />
            </ShortcutContext.Provider>
          </GlobalWrapper>
        </Route>

        <Route path="/">
          <Landing />
        </Route>
      </Switch>
    </Router>
  );
};
