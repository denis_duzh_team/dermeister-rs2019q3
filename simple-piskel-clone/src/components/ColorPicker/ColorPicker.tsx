import React from "react";
import styled from "styled-components";
import { pipe, get } from "lodash/fp";

import { RGBAColor } from "../../types/types";
import { hexToRGBA, RGBAToHex } from "../../utils/Common";

import bgLightTransparent from "../../img/light-transparent.png";

type OnColor = (color: RGBAColor) => void;

interface ColorPickerProps {
  className?: string;
  primary: RGBAColor;
  secondary: RGBAColor;
  onPrimary: OnColor;
  onSecondary: OnColor;
}

interface LabelProps {
  _color: RGBAColor;
}

const FlexWrapper = styled.div`
  display: flex;
  justify-content: space-between;

  width: 82px;
`;

const Wrapper = styled.div`
  width: 40px;
  height: 40px;

  background-image: url(${bgLightTransparent});
`;

const Input = styled.input`
  display: none;
`;

const Label = styled.label`
  display: block;
  width: 100%;
  height: 100%;

  background-color: ${({ _color: { r, g, b, a } }: LabelProps) => `rgba(${r}, ${g}, ${b}, ${a})`};
  border: 3px solid #444444;
  cursor: pointer;

  &:hover {
    border-color: #3a3a3a;
  }
`;

export default ({ primary, secondary, onPrimary, onSecondary, className }: ColorPickerProps) => {
  const getColor = pipe(get("target.value"), hexToRGBA);

  return (
    <FlexWrapper className={className}>
      <Wrapper>
        <Label htmlFor="primary-color" _color={primary}></Label>
        <Input
          type="color"
          id="primary-color"
          value={RGBAToHex(primary)}
          onChange={pipe(getColor, onPrimary)}
        />
      </Wrapper>
      <Wrapper>
        <Label htmlFor="secondary-color" _color={secondary}></Label>
        <Input
          type="color"
          id="secondary-color"
          value={RGBAToHex(secondary)}
          onChange={pipe(getColor, onSecondary)}
        />
      </Wrapper>
    </FlexWrapper>
  );
};
