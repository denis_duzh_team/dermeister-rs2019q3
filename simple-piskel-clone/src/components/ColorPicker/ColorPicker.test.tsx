import React from "react";
import { render } from "@testing-library/react";
import ColorPicker from "./ColorPicker";
import { RGBAColor } from "../../types/types";

test("renders ColorPicker", () => {
  const onPrimary = (color: RGBAColor) => {};
  const primary = { r: 0, g: 0, b: 0, a: 0 };
  const onSecondary = (color: RGBAColor) => {};
  const secondary = { r: 0, g: 0, b: 0, a: 0 };

  const renderResult = render(
    <ColorPicker
      onPrimary={onPrimary}
      onSecondary={onSecondary}
      primary={primary}
      secondary={secondary}
    />
  );

  expect(renderResult).toBeDefined();
});
