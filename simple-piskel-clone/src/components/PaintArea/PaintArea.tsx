import React, { createRef } from "react";
import styled from "styled-components";

import {
  Frame,
  Tool,
  PaintConfig,
  PaintAction,
  PaintActionType,
  PaintFactory,
  RGBAColor
} from "../../types/types";
import { useCanvas } from "../../hooks/hooks";
import { createPaintFactory } from "../../utils/Draw";
import { ToolType } from "../../types/types";
import defaults from "../../config/Defaults";

import bgDarkTransparent from "../../img/dark-transparent.png";

interface PaintAreaProps {
  className?: string;
  tool: Tool;
  config: PaintConfig;
  frame: Frame;
  onAction: (action: PaintAction) => void;
}

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;

  background-color: #a0a0a0;
`;

const Canvas = styled.canvas`
  margin: auto;
  width: ${defaults.paintAreaActualSize}px;
  height: ${defaults.paintAreaActualSize}px;

  background-image: url(${bgDarkTransparent});
  image-rendering: pixelated;
`;

let paintFactory: PaintFactory | null;

export default ({ config, frame, tool, onAction, className }: PaintAreaProps) => {
  const { paintAreaSize } = config;
  const canvasRef = createRef<HTMLCanvasElement>();

  useCanvas([canvasRef], [frame], paintAreaSize);

  const scale = (x: number, y: number) => ({
    x: Math.floor(x / (defaults.paintAreaActualSize / paintAreaSize)),
    y: Math.floor(y / (defaults.paintAreaActualSize / paintAreaSize))
  });

  const onNewFrame = (frame?: Frame) =>
    frame && onAction({ type: PaintActionType.NEW_FRAME, value: frame });

  const onNewColor = (color: RGBAColor) =>
    onAction({ type: PaintActionType.NEW_COLOR, value: color });

  const onMouseDown = ({ nativeEvent: { offsetX: x, offsetY: y } }: React.MouseEvent) => {
    const scaled = scale(x, y);
    x = scaled.x;
    y = scaled.y;
    paintFactory = createPaintFactory(frame, { x, y }, config);

    switch (tool.type) {
      case ToolType.PEN:
      case ToolType.STROKE:
        onNewFrame(paintFactory.line({ x, y }));
        break;

      case ToolType.PAINT_BUCKET:
        onNewFrame(paintFactory.fill());
        break;

      case ToolType.COLOR_SELECT:
        onNewColor(paintFactory.color());
        break;

      case ToolType.ERASER:
        onNewFrame(paintFactory.erase({ x, y }));
        break;

      case ToolType.ALL_PIXELS_SAME_COLOR:
        onNewFrame(paintFactory.sameColor());
        break;
    }
  };

  const onMouseMove = ({ nativeEvent: { offsetX: x, offsetY: y } }: React.MouseEvent) => {
    const scaled = scale(x, y);
    x = scaled.x;
    y = scaled.y;

    switch (tool.type) {
      case ToolType.PEN:
        onNewFrame(paintFactory?.line({ x, y }));
        break;

      case ToolType.ERASER:
        onNewFrame(paintFactory?.erase({ x, y }));
        break;

      case ToolType.STROKE:
        onNewFrame(paintFactory?.stroke({ x, y }));
        break;
    }
  };

  return (
    <Wrapper className={className}>
      <Canvas
        ref={canvasRef}
        width={paintAreaSize}
        height={paintAreaSize}
        onMouseDown={onMouseDown}
        onMouseMove={onMouseMove}
        onMouseUp={() => (paintFactory = null)}
      ></Canvas>
    </Wrapper>
  );
};
