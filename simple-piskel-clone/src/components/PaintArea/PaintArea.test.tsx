import React from "react";
import { render } from "@testing-library/react";
import PaintArea from "./PaintArea";
import { ToolType } from "../../types/types";

test("renders PaintArea", () => {
  //@ts-ignore
  HTMLCanvasElement.prototype.getContext = () => {};
  const renderResult = render(
    <PaintArea
      tool={{
        type: ToolType.PEN,
        offsetX: -184,
        offsetY: -93
      }}
      frame={Uint8ClampedArray.from(Array(2 * 2 * 4))}
      config={{ paintAreaSize: 2, penSize: 1, color: { r: 0, g: 0, b: 0, a: 0 } }}
      onAction={() => {}}
    />
  );

  expect(renderResult).toBeDefined();
});
