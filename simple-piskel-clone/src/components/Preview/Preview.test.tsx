import React from "react";
import { render } from "@testing-library/react";
import Preview from "./Preview";

test("renders Preview", () => {
  //@ts-ignore
  HTMLCanvasElement.prototype.getContext = () => {};
  const renderResult = render(<Preview fps={1} frames={[]} onFps={() => {}} size={2} />);

  expect(renderResult).toBeDefined();
});
