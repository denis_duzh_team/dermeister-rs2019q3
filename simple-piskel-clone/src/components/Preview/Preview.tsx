import React, { createRef } from "react";
import styled from "styled-components";
import { pipe, get } from "lodash/fp";

import { Frame } from "../../types/types";
import defaults from "../../config/Defaults";
import { useCanvas, useFramesPreview } from "../../hooks/hooks";

import bgDarkTransparent from "../../img/dark-transparent.png";

interface PreviewProps {
  className?: string;
  frames: Frame[];
  size: number;
  fps: number;
  onFps: (fps: number) => void;
}

const Wrapper = styled.div`
  width: 200px;
`;

const FlexWrapper = styled.div`
  display: flex;

  background-color: #333333;
`;

const Canvas = styled.canvas`
  width: ${defaults.previewSize}px;
  height: ${defaults.previewSize}px;

  background-image: url(${bgDarkTransparent});
  image-rendering: pixelated;
  vertical-align: bottom;
  cursor: pointer;
`;

const FPS = styled.p`
  width: 75px;
  margin: 0;
  padding: 5px 10px;

  text-align: right;

  background-color: #333333;
  border-right: 1px solid #444444;
`;

const Input = styled.input`
  margin-left: 10px;
  width: 100px;
`;

export default ({ frames, size, className, fps, onFps }: PreviewProps) => {
  const canvasRef = createRef<HTMLCanvasElement>();

  const currentFrame = useFramesPreview(frames, fps);
  useCanvas([canvasRef], [frames[currentFrame]], size);

  const onClick = async ({ target }: React.MouseEvent) =>
    (target as HTMLDivElement).requestFullscreen();

  return (
    <Wrapper className={className}>
      <Canvas ref={canvasRef} width={size} height={size} onClick={onClick}></Canvas>
      <FlexWrapper>
        <FPS>{fps} FPS</FPS>
        <Input
          type="range"
          min={defaults.lowFps}
          max={defaults.highFps}
          value={fps}
          onChange={pipe(get("target.value"), Number, onFps)}
        />
      </FlexWrapper>
    </Wrapper>
  );
};
