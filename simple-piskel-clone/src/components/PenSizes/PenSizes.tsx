import React from "react";
import styled from "styled-components";
import { pipe, get } from "lodash/fp";

import { penSizes } from "../../config/PenSizes";

interface PenSizesProps {
  className?: string;
  penSize: number;
  onPenSize: (size: number) => void;
}

interface LabelProps {
  size: number;
  checked: boolean;
}

const PenSizes = styled.ul`
  display: flex;

  margin: 0;
  padding: 0;

  list-style: none;
`;

const Label = styled.label`
  position: relative;

  display: block;
  margin-right: 2px;
  width: 20px;
  height: 20px;

  border: 2px solid ${({ checked }: LabelProps) => (checked ? "#ffd700" : "#444444")};
  cursor: pointer;

  &::before {
    content: "";

    position: absolute;
    left: 50%;
    top: 50%;
    transform: translate(-50%, -50%);

    width: ${({ size }: LabelProps) => 6 + (size - 1) * 2}px;
    height: ${({ size }: LabelProps) => 6 + (size - 1) * 2}px;

    background-color: ${({ checked }: LabelProps) => (checked ? `#ffd700` : `white`)};
  }

  &:hover {
    border-color: #888888;
  }
`;

const Input = styled.input`
  display: none;
`;

export default ({ penSize, onPenSize, className }: PenSizesProps) => (
  <PenSizes className={className}>
    {penSizes.map(({ value }) => {
      const penSizeId = `pen-size-${value}`;

      return (
        <li key={value}>
          <Label htmlFor={penSizeId} size={value} checked={value === penSize}></Label>
          <Input
            type="radio"
            id={penSizeId}
            name="pen-sizes"
            value={value}
            onChange={pipe(get("target.value"), Number, onPenSize)}
            checked={value === penSize}
          />
        </li>
      );
    })}
  </PenSizes>
);
