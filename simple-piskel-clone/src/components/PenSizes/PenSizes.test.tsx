import React from "react";
import { render } from "@testing-library/react";
import PenSizes from "./PenSizes";

test("renders PenSizes", () => {
  const renderResult = render(<PenSizes penSize={0} onPenSize={() => {}} />);

  expect(renderResult).toBeDefined();
});
