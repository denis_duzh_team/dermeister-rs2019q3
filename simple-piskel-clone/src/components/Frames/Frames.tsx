import React, { createRef, useState, useContext } from "react";
import styled from "styled-components";

import { Frame, FrameAction, ShortcutSection, FrameShortcutActionType } from "../../types/types";
import { useCanvas, useShortcuts } from "../../hooks/hooks";
import { initFrame } from "../../utils/Draw";
import defaults from "../../config/Defaults";
import { ShortcutContext } from "../../context/context";

import bgDarkTransparent from "../../img/dark-transparent.png";
import icons from "../../img/icons.png";

interface FramesProps {
  className?: string;
  frames: Frame[];
  currentFrame: number;
  paintAreaSize: number;
  onFrameAction: (action: FrameAction) => void;
}

interface FrameItemProps {
  current: boolean;
}

const Wrapper = styled.div`
  width: 106.5px;
`;

const FrameList = styled.ul`
  display: flex;
  flex-direction: column;

  margin: 0;
  padding: 0;

  list-style: none;
`;

const FrameItem = styled.li`
  position: relative;

  margin-bottom: 20px;

  border: 5px solid ${({ current }: FrameItemProps) => (current ? "#ffd700" : "#444444")};
  cursor: pointer;

  &:hover button {
    display: block;
  }
`;

const Button = styled.button`
  position: absolute;
  width: 30px;
  height: 30px;

  display: none;
  margin: 0;
  padding: 0;

  border: none;
  background: url(${icons}) rgba(100, 100, 100, 0.6) no-repeat;
  outline: none;
  cursor: pointer;
`;

const Duplicate = styled(Button)`
  right: 1px;
  bottom: 1px;

  background-position: -274px -76px;
`;

const Delete = styled(Button)`
  right: 1px;
  top: 1px;
  background-position: -274px -106px;
`;

const Canvas = styled.canvas`
  width: ${defaults.frameSize}px;
  height: ${defaults.frameSize}px;

  background-image: url(${bgDarkTransparent});
  image-rendering: pixelated;
  vertical-align: bottom;
`;

const NewFrame = styled.button`
  position: relative;

  margin: 0;
  padding: 7px 0;
  padding-left: 35px;
  width: 100%;

  font-size: 13px;
  text-align: left;
  color: #888888;

  border: 3px solid #888888;
  border-radius: 3px;
  background: none;
  cursor: pointer;
  outline: none;

  &::before {
    content: "";

    position: absolute;
    left: 5px;
    top: 9px;

    width: 26px;
    height: 26px;

    background: url(${icons}) no-repeat -274px -157px;
  }

  &:hover {
    border-color: #ffd700;
  }
`;

export default ({ frames, currentFrame, paintAreaSize, onFrameAction, className }: FramesProps) => {
  const refs = frames.map(() => createRef<HTMLCanvasElement>());
  const [draggedOverFrame, setDraggedOverFrame] = useState<Frame>();
  const [draggedFrame, setDraggedFrame] = useState<Frame>();

  const onDragOver = (index: number) => (e: React.DragEvent) => {
    e.preventDefault();

    if (draggedOverFrame !== frames[index]) setDraggedOverFrame(frames[index]);
  };

  const onDrop = () => {
    const draggedFrameIndex = frames.indexOf(draggedFrame as Frame);
    const draggedOverFrameIndex = frames.indexOf(draggedOverFrame as Frame);

    frames.splice(draggedFrameIndex, 1);
    frames.splice(draggedOverFrameIndex, 0, draggedFrame as Frame);

    onFrameAction({ frames: frames.slice(0), current: draggedOverFrameIndex });
  };

  const onDeleteFrame = (index: number) => (e?: React.MouseEvent) => {
    e?.stopPropagation();

    frames.splice(index, 1);
    onFrameAction({
      frames: frames.slice(0),
      current: index > currentFrame ? currentFrame : currentFrame - 1
    });
  };

  const onSelectFrame = (index: number) => () => onFrameAction({ current: index });

  const onDuplicateFrame = (index: number) => (e?: React.MouseEvent) => {
    e?.stopPropagation();

    const dup = frames[index].slice(0);
    frames.splice(index, 0, dup);

    onFrameAction({ frames: frames.slice(0), current: index + 1 });
  };

  const onNewFrame = () => {
    const _frame = initFrame(paintAreaSize, paintAreaSize);
    frames.push(_frame);

    onFrameAction({
      frames: frames.slice(0),
      current: frames.length - 1
    });
  };

  const section = useContext<ShortcutSection[]>(ShortcutContext).find(
    ({ sectionName }) => sectionName === "frames"
  );

  useCanvas(refs, frames, paintAreaSize);

  useShortcuts(section, (type: FrameShortcutActionType) => {
    switch (type) {
      case FrameShortcutActionType.CREATE:
        onNewFrame();
        break;

      case FrameShortcutActionType.DUPLICATE:
        onDuplicateFrame(currentFrame)();
        break;

      case FrameShortcutActionType.DELETE:
        currentFrame > 0 && onDeleteFrame(currentFrame)();
        break;
    }
  });

  return (
    <Wrapper className={className}>
      <FrameList>
        {refs.map((ref, index) => {
          return (
            <FrameItem
              key={index}
              onClick={onSelectFrame(index)}
              onDragOver={onDragOver(index)}
              onDragStart={() => setDraggedFrame(frames[index])}
              onDrop={onDrop}
              current={index === currentFrame}
              draggable
            >
              {index > 0 && <Delete onClick={onDeleteFrame(index)} />}
              <Duplicate onClick={onDuplicateFrame(index)} />
              <Canvas ref={ref} width={paintAreaSize} height={paintAreaSize}></Canvas>
            </FrameItem>
          );
        })}
      </FrameList>

      <NewFrame onClick={onNewFrame}>Add new frame</NewFrame>
    </Wrapper>
  );
};
