import React from "react";
import { render } from "@testing-library/react";
import Frames from "./Frames";
import { FrameAction } from "../../types/types";

test("renders Frames", () => {
  const onFrameAction = (action: FrameAction) => {};
  const renderResult = render(
    <Frames frames={[]} currentFrame={0} onFrameAction={onFrameAction} paintAreaSize={4} />
  );

  expect(renderResult).toBeDefined();
});
