import React from "react";
import { Link } from "react-router-dom";
import styled from "styled-components";
import { GoogleLogin, GoogleLogout } from "react-google-login";

import defaults from "../../config/Defaults";

interface NavbarProps {
  className?: string;
  userLogged: boolean;
  onUserLogged: (userLogged: boolean) => void;
}

const Navbar = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;

  padding: 10px 10px;
  width: 100%;
  height: 50px;

  background-color: #2d2d2d;
`;

const Button = styled.button`
  color: black;
  text-align: center;
  font-size: 16px;

  background-color: gold;
  border: none;
  border-radius: 2px;
  cursor: pointer;
  outline: none;

  &:hover {
    background-color: white;
  }
`;

const StyledLink = styled(Link)`
  display: block;
  padding: 2px 7px;

  color: black;
  text-align: center;
  font-size: 16px;
  text-decoration: none;

  background-color: gold;
  border: none;
  border-radius: 2px;
  cursor: pointer;
  outline: none;

  &:hover {
    background-color: white;
  }
`;

export default ({ userLogged, onUserLogged, className }: NavbarProps) => (
  <Navbar className={className}>
    <StyledLink to="/">Home</StyledLink>
    {!userLogged ? (
      <GoogleLogin
        clientId={defaults.oauthId}
        onSuccess={() => onUserLogged(true)}
        onFailure={() => {}}
        cookiePolicy={"single_host_origin"}
        render={(renderProps: any) => <Button onClick={renderProps.onClick}>Log in</Button>}
      />
    ) : (
      <GoogleLogout
        clientId={defaults.oauthId}
        onLogoutSuccess={() => onUserLogged(false)}
        render={(renderProps: any) => <Button onClick={renderProps.onClick}>Log out</Button>}
      />
    )}
  </Navbar>
);
