import React from "react";
import { render } from "@testing-library/react";
import Navbar from "./Navbar";
import { Route, BrowserRouter as Router, Switch } from "react-router-dom";

test("renders Navbar", () => {
  const renderResult = render(
    <Router>
      <Switch>
        <Route>
          <Navbar userLogged={true} onUserLogged={() => {}} />
        </Route>
      </Switch>
    </Router>
  );

  expect(renderResult).toBeDefined();
});
