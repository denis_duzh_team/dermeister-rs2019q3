import React, { useContext } from "react";
import styled from "styled-components";
import { pipe, get } from "lodash/fp";

import { Tool, ShortcutSection } from "../../types/types";
import { getToolByType } from "../../utils/Common";
import { tools } from "../../config/Tools";
import { useShortcuts } from "../../hooks/hooks";

import { ShortcutContext } from "../../context/context";

import icons from "../../img/icons.png";

interface PaintToolsProps {
  className?: string;
  current: Tool;
  onTool: (tool: Tool) => void;
}

interface LabelProps {
  checked: boolean;
  offsetX: number;
  offsetY: number;
}

const PaintTools = styled.ul`
  display: flex;
  justify-content: space-between;
  flex-wrap: wrap;

  margin: 0;
  padding: 0;
  width: 94px;

  list-style: none;
`;

const PaintTool = styled.li``;

const Label = styled.label`
  display: block;
  margin-bottom: 2px;
  width: 46px;
  height: 46px;

  background: url(${icons}) #3a3a3a no-repeat;
  background-position-x: ${({ offsetX }: LabelProps) => offsetX}px;
  background-position-y: ${({ offsetY }: LabelProps) => offsetY}px;
  cursor: pointer;
  border: 2px solid ${({ checked }: LabelProps) => (checked ? "#ffd700" : "#3a3a3a")};
`;

const Input = styled.input`
  display: none;
`;

export default ({ current, onTool, className }: PaintToolsProps) => {
  const onChange = pipe(getToolByType(tools), onTool);

  const section = useContext<ShortcutSection[]>(ShortcutContext).find(
    ({ sectionName }) => sectionName === "tools"
  );

  useShortcuts(section, onChange);

  return (
    <PaintTools className={className}>
      {tools.map(({ type, offsetX, offsetY }: Tool, index: number) => {
        const toolId = `tool-${index}`;
        return (
          <PaintTool key={index}>
            <Label
              htmlFor={toolId}
              offsetX={offsetX}
              offsetY={offsetY}
              checked={type === current.type}
            ></Label>
            <Input
              type="radio"
              id={toolId}
              name="tools"
              value={type}
              onChange={pipe(get("target.value"), onChange)}
              checked={type === current.type}
            />
          </PaintTool>
        );
      })}
    </PaintTools>
  );
};
