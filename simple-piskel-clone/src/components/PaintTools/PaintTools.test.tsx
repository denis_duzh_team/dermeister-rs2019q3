import React from "react";
import { render } from "@testing-library/react";
import PaintTools from "./PaintTools";
import { ToolType } from "../../types/types";

test("renders PaintTools", () => {
  const renderResult = render(
    <PaintTools
      current={{
        type: ToolType.PEN,
        offsetX: -184,
        offsetY: -93
      }}
      onTool={() => {}}
    />
  );

  expect(renderResult).toBeDefined();
});
