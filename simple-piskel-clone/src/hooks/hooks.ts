import { useEffect, useState } from "react";

import { NullableHTMLCanvasElement, Frame, ShortcutSection } from "../types/types";

export const useCanvas = (refs: any[], frames: Frame[], size: number) => {
  useEffect(() => {
    refs.forEach((ref, i) => {
      const frame = frames[i];
      const canvas = ref.current as NullableHTMLCanvasElement;
      const ctx = canvas?.getContext("2d");
      ctx?.putImageData(new ImageData(frame, size, size), 0, 0);
    });
  });
};

export const useFramesPreview = (frames: Frame[], fps: number) => {
  const [currentFrame, setCurrentFrame] = useState<number>(0);

  useEffect(() => {
    const interval = 1000 / fps;

    if (interval !== Infinity) {
      const intervalId = setInterval(
        () => setCurrentFrame((currentFrame + 1) % frames.length),
        interval
      );
      return () => clearInterval(intervalId);
    }
  });

  return currentFrame % frames.length;
};

export const useShortcuts = (section: ShortcutSection | undefined, cb: any) => {
  useEffect(() => {
    if (section) {
      const listener = ({ code }: KeyboardEvent) =>
        section.shortcuts.forEach(({ keys, value }) => code === keys && cb(value));

      document.addEventListener("keypress", listener);
      return () => document.removeEventListener("keypress", listener);
    }
  });
};
