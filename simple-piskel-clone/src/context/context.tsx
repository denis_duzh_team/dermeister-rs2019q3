import React from "react";
import { ShortcutSection } from "../types/types";

export const ShortcutContext = React.createContext([] as ShortcutSection[]);
