const semiFact = function(value) {
  value = Number(value);
  const result = { two: 0, five: 0 };

  let base = 10;
  while (base / 2 <= value + 5) {
    if (value % 2 === 0) {
      result.five += Math.floor(value / base);
    } else {
      result.five += Math.floor(value / base + 0.5);
    }
    base *= 5;
  }

  base = 2;
  while (value % 2 === 0 && base <= value) {
    result.two += Math.floor(value / base);
    base *= 2;
  }

  return result;
};

const fact = function(value) {
  const semiA = semiFact(value);
  const semiB = semiFact(value - 1);

  return {
    two: semiA.two + semiB.two,
    five: semiA.five + semiB.five
  };
};

module.exports = function zeros(expression) {
  const factorials = expression
    .split("*")
    .map(value => (value.endsWith("!!") ? semiFact(value.slice(0, -2)) : fact(value.slice(0, -1))));

  const power = factorials.reduce(
    (prev, curr) => {
      prev.two += curr.two;
      prev.five += curr.five;
      return prev;
    },
    { two: 0, five: 0 }
  );
  return Math.min(power.two, power.five);
};
