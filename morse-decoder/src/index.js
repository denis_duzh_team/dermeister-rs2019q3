const MORSE_TABLE = {
  ".-": "a",
  "-...": "b",
  "-.-.": "c",
  "-..": "d",
  ".": "e",
  "..-.": "f",
  "--.": "g",
  "....": "h",
  "..": "i",
  ".---": "j",
  "-.-": "k",
  ".-..": "l",
  "--": "m",
  "-.": "n",
  "---": "o",
  ".--.": "p",
  "--.-": "q",
  ".-.": "r",
  "...": "s",
  "-": "t",
  "..-": "u",
  "...-": "v",
  ".--": "w",
  "-..-": "x",
  "-.--": "y",
  "--..": "z",
  ".----": "1",
  "..---": "2",
  "...--": "3",
  "....-": "4",
  ".....": "5",
  "-....": "6",
  "--...": "7",
  "---..": "8",
  "----.": "9",
  "-----": "0"
};

const encodeKey = function(key) {
  return key
    .split("")
    .map(char => (char === "." ? "10" : "11"))
    .join("")
    .padStart(10, "0");
};

const getEncodedMorse = function(morseTable) {
  const encodedMorse = { "**********": " " };

  return Object.keys(morseTable).reduce((table, key) => {
    table[encodeKey(key)] = morseTable[key];
    return encodedMorse;
  }, encodedMorse);
};

function decode(expr) {
  const buffer = [];
  expr.split("").reduce((chunk, current) => {
    if (chunk.length === 9) {
      buffer.push(chunk + current);
      return "";
    }
    return chunk + current;
  }, "");

  const encodedMorse = getEncodedMorse(MORSE_TABLE);
  return buffer.map(chunk => encodedMorse[chunk]).join("");
}

module.exports = {
  decode
};
