const showDesc = document.querySelector('.btn-description');
const descriptions = document.querySelectorAll('.project__description');

showDesc.onclick = () =>
  Array.from(descriptions).map(desc =>
    desc.classList.toggle('project__description--hidden')
  );
