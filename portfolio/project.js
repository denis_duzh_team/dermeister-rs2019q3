const projects = [
  {
    name: 'theyalow',
    mobileWidth: 640,
    desktopWidth: 1300,
    src: 'projects/theyalow/index.html'
  },
  {
    name: 'repair-design-project',
    mobileWidth: 375,
    desktopWidth: 1440,
    src: 'projects/repair-design-project/index.html'
  }
];

const projectName = new URLSearchParams(window.location.search).get('name');
const { desktopWidth, mobileWidth, src } = projects.find(
  project => project.name === projectName
);

const iframe = document.createElement('iframe');
iframe.src = src;
iframe.style.width = '100vw';
document.body.appendChild(iframe);

const createBtn = (text, onclick) => {
  const btn = document.createElement('button');
  btn.textContent = text;
  btn.classList.add('btn');
  btn.onclick = onclick;

  return btn;
};

const viewBtnClick = e => {
  const isDesktopMedia = iframe.clientWidth >= desktopWidth;
  iframe.style.width = isDesktopMedia ? `${mobileWidth}px` : '100vw';
  e.target.textContent = isDesktopMedia ? 'Desktop' : 'Mobile';
};
const viewBtn = createBtn('Mobile', viewBtnClick);
const backBtn = createBtn('Back', () => (document.location.href = '/'));

window.onresize = () => {
  if (window.innerWidth < desktopWidth) {
    viewBtn.style.display = 'none';
    if (iframe.style.width !== '100vw') iframe.style.width = '100vw';
  } else {
    viewBtn.style.display = 'block';
  }
};
window.onresize();

const btnGroup = document.createElement('div');
btnGroup.classList.add('btn-group');
btnGroup.appendChild(viewBtn);
btnGroup.appendChild(backBtn);
document.body.appendChild(btnGroup);
