function eval() {
  // Do not use eval!!!
  return;
}

const toPolish = function(tokens) {
  const priority = {
    "*": 2,
    "/": 2,
    "+": 1,
    "-": 1
  };

  const stack = [];
  let result = "";

  for (const token of tokens) {
    if (Number.isInteger(+token)) result += `${token} `;

    if (token === "(") stack.push(token);
    if (token === ")") {
      while (stack[stack.length - 1] !== "(") {
        if (stack.length === 0) throw new Error("ExpressionError: Brackets must be paired");
        result += `${stack.pop()} `;
      }
      stack.pop();
    }

    if (token in priority) {
      while (priority[stack[stack.length - 1]] >= priority[token]) {
        result += `${stack.pop()} `;
      }
      stack.push(token);
    }
  }

  while (stack.length > 0) {
    if (stack[stack.length - 1] === "(")
      throw new Error("ExpressionError: Brackets must be paired");
    result += `${stack.pop()} `;
  }

  return result;
};

const evaluate = expr => {
  const operators = {
    "+": (x, y) => x + y,
    "-": (x, y) => x - y,
    "*": (x, y) => x * y,
    "/": (x, y) => {
      if (y == 0) throw new Error("TypeError: Devision by zero.");
      return x / y;
    }
  };

  let stack = [];
  expr
    .split(" ")
    .filter(token => Boolean(token))
    .forEach(token => {
      if (token in operators) {
        let [y, x] = [stack.pop(), stack.pop()];
        stack.push(operators[token](x, y));
      } else {
        stack.push(parseFloat(token));
      }
    });

  return stack.pop();
};

const normalizeExpr = function(polish) {
  const operatorSymbols = ["*", "-", "+", "/", "(", ")"];
  return polish
    .split("")
    .map(token => (operatorSymbols.includes(token) ? ` ${token} ` : token))
    .join("")
    .split(" ")
    .filter(token => token !== "");
};

function expressionCalculator(expr) {
  return evaluate(toPolish(normalizeExpr(expr)));
}

module.exports = {
  expressionCalculator
};
