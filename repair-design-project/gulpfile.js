const { src, dest, series, parallel, watch } = require("gulp");

const plumber = require("gulp-plumber");
const sass = require("gulp-sass");
sass.compiler = require("node-sass");
const browserSync = require("browser-sync").create();

async function css() {
  src("src/scss/style.scss")
    .pipe(plumber())
    .pipe(sass())
    .pipe(dest("dist/"))
    .pipe(browserSync.stream());
}

async function html() {
  src("src/*.html").pipe(dest("dist/"));
}

async function assets() {
  src("src/assets/**/*").pipe(dest("dist/assets"));
}

async function browserSyncInit() {
  browserSync.init({
    server: {
      baseDir: "./dist/"
    }
  });
}

async function browserSyncReload() {
  browserSync.reload();
}

async function watchAll() {
  watch("src/scss/**/*.scss", css);
  watch("src/*.html", series(html, browserSyncReload));
  watch("src/assets/*", series(assets, browserSyncReload));
}

exports.default = series(parallel(css, html, assets), watchAll, browserSyncInit);
