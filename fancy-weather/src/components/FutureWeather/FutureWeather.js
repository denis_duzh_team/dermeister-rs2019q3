import RS from '../RS';

import getLocale from '../../utils/LocationUtils';
import { getDegrees } from '../../utils/WeatherUtils';

import './FutureWeather.scss';

export default class FutureWeather extends RS.Component {
  constructor(props) {
    super(props);

    this.ref = RS.createRef();
  }

  componentDidMount() {
    this.ref.current.style.setProperty('--bg-image-url', `url(${this.props.image})`);
  }

  componentDidUpdate() {
    this.ref.current.style.setProperty('--bg-image-url', `url(${this.props.image})`);
  }

  render() {
    const { date, degrees } = this.props.weather;
    return (
      <div className="FutureWeather" ref={this.ref}>
        <p className="FutureWeather-date">
          {date.toLocaleString(getLocale(this.props.context.language), { weekday: 'long' })}
        </p>
        <p className="FutureWeather-degrees">{Math.round(getDegrees(degrees, this.props.units)).toString()}</p>
      </div>
    );
  }
}
