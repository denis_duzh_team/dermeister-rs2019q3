import RS from '../RS';

import './LanguageSwitch.scss';

const languageConfig = [
  {
    title: 'en',
    value: 'en',
  },
  {
    title: 'ru',
    value: 'ru',
  },
  {
    title: 'be',
    value: 'be',
  },
];

export default class LanguageSwitch extends RS.Component {
  onClick(e) {
    this.props.onLanguage(e.target.value);
  }

  render() {
    return (
      <select className={`${this.props.className} LanguageSwitch`} onchange={(e) => this.onClick(e)}>
        {languageConfig.map((language) => (
          <option value={language.value} selected={language.value === this.props.context.language}>
            {language.title}
          </option>
        ))}
      </select>
    );
  }
}
