import RS from '../RS';

import TodayWeather from '../TodayWeather/TodayWeather';
import FutureWeather from '../FutureWeather/FutureWeather';

import './Weather.scss';

import _01d from './img/01d.svg';
import _02d from './img/02d.svg';
import _03d from './img/03d.svg';
import _04d from './img/04d.svg';
import _09d from './img/09d.svg';
import _10d from './img/10d.svg';
import _11d from './img/11d.svg';
import _13d from './img/13d.svg';
import _50d from './img/50d.svg';
import _01n from './img/01n.svg';
import _02n from './img/02n.svg';
import _03n from './img/03n.svg';
import _04n from './img/04n.svg';
import _09n from './img/09n.svg';
import _10n from './img/10n.svg';
import _11n from './img/11n.svg';
import _13n from './img/13n.svg';
import _50n from './img/50n.svg';

const images = {
  '01d': _01d,
  '02d': _02d,
  '03d': _03d,
  '04d': _04d,
  '09d': _09d,
  '10d': _10d,
  '11d': _11d,
  '13d': _13d,
  '50d': _50d,
  '01n': _01n,
  '02n': _02n,
  '03n': _03n,
  '04n': _04n,
  '09n': _09n,
  '10n': _10n,
  '11n': _11n,
  '13n': _13n,
  '50n': _50n,
};

export default class Weather extends RS.Component {
  render() {
    return (
      <div className={`${this.props.className} Weather`}>
        <TodayWeather
          className="Weather-today"
          weather={this.props.forecast.current}
          units={this.props.units}
          image={images[this.props.forecast.current.image]}
          context={this.props.context}
        />
        {this.props.forecast.future.map((weather) => (
          <FutureWeather
            weather={weather}
            units={this.props.units}
            image={images[weather.image]}
            context={this.props.context}
          />
        ))}
      </div>
    );
  }
}
