const i18n = {
  SEARCH_PLACEHOLDER: {
    en: 'Search city',
    ru: 'Поиск города',
    be: 'Пошук горада',
  },
  VOICE_SEARCH_PLACEHOLDER: {
    en: 'Start speaking...',
    ru: 'Говорите...',
    be: 'Гаварыце...',
  },
  SEARCH_BUTTON: {
    en: 'Search',
    ru: 'Поиск',
    be: 'Пошук',
  },
};

export default (language) => ({
  SEARCH_PLACEHOLDER: i18n.SEARCH_PLACEHOLDER[language],
  VOICE_SEARCH_PLACEHOLDER: i18n.VOICE_SEARCH_PLACEHOLDER[language],
  SEARCH_BUTTON: i18n.SEARCH_BUTTON[language],
});
