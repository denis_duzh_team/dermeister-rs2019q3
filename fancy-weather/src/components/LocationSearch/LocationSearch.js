import RS from '../RS';

import { getLocationByCity } from '../../services/LocationService';
import getLocale from '../../utils/LocationUtils';

import toI18n from './i18n';

import './LocationSearch.scss';

export default class LocationSearch extends RS.Component {
  constructor(props) {
    super(props);

    this.state = {
      searchText: '',
    };

    const SpeechRecognition = window.SpeechRecognition || window.webkitSpeechRecognition;
    this.recognition = new SpeechRecognition();

    this.recognition.onspeechend = () => this.recognition.stop();
    this.recognition.onresult = (evt) => {
      const last = evt.results.length - 1;
      const city = evt.results[last][0].transcript;
      this.setState({ searchText: city });
      this.onSearch();
    };
  }

  onInput(e) {
    this.setState({ searchText: e.target.value });
  }

  async onSearch() {
    try {
      const location = await getLocationByCity(this.state.searchText, this.props.context.language);
      this.props.onLocation(location);
    } catch (e) {}
  }

  onVoice() {
    const { language } = this.props.context;
    this.setState({ searchText: toI18n(language).VOICE_SEARCH_PLACEHOLDER });
    this.recognition.lang = getLocale(language);
    this.recognition.start();
  }

  render() {
    const { SEARCH_PLACEHOLDER, SEARCH_BUTTON } = toI18n(this.props.context.language);

    return (
      <div className={`${this.props.className} LocationSearch`}>
        <button className="LocationSearch-voice" onclick={() => this.onVoice()}>
          Search voice
        </button>
        <input
          className="LocationSearch-input"
          value={this.state.searchText}
          oninput={(e) => this.onInput(e)}
          placeholder={SEARCH_PLACEHOLDER}
        />
        <button className="LocationSearch-button" onclick={() => this.onSearch()}>
          {SEARCH_BUTTON}
        </button>
      </div>
    );
  }
}
