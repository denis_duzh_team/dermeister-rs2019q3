import RS from '../RS';

import './Spinner.scss';

export default class Spinner extends RS.Component {
  getClassName() {
    return this.props.visible ? 'Spinner' : 'Spinner Spinner-hidden';
  }

  render() {
    return <div className={this.getClassName()}>Spinning</div>;
  }
}
