import RS from '../RS';

import Spinner from '../Spinner/Spinner';
import BgSwitch from '../BgSwitch/BgSwitch';
import LanguageSwitch from '../LanguageSwitch/LanguageSwitch';
import UnitSwitch from '../UnitSwitch/UnitSwitch';
import LocationSearch from '../LocationSearch/LocationSearch';
import LocationInfo from '../LocationInfo/LocationInfo';
import LocationMap from '../LocationMap/LocationMap';
import Weather from '../Weather/Weather';

import { extractWeatherKeywords } from '../../utils/WeatherUtils';
import getImage from '../../services/ImageService';
import getForecast from '../../services/WeatherService';
import { getLocationByCoords, getLocationByCity } from '../../services/LocationService';

import './App.scss';

const initialState = {
  bgUrl: '',
  units: localStorage.getItem('units') || 'c',
  context: {
    language: localStorage.getItem('language') || 'en',
  },
  forecast: {
    current: {
      degrees: 0,
      description: '',
      feels: 0,
      wind: 0,
      humidity: 0,
      date: new Date(),
      image: '',
    },
    future: [],
  },
  location: {
    country: '',
    city: '',
    longitude: 0,
    latitude: 0,
    timezoneOffset: 0,
  },
  contentLoading: true,
};

export default class App extends RS.Component {
  constructor(props) {
    super(props);

    this.state = initialState;
    this.appRef = RS.createRef();
  }

  async componentDidMount() {
    const {
      coords: { longitude, latitude },
    } = await new Promise((resolve) => navigator.geolocation.getCurrentPosition(resolve));

    const location = await getLocationByCoords(longitude, latitude, this.state.context.language);
    const forecast = await getForecast(longitude, latitude, this.state.context.language);
    let bgUrl;
    try {
      bgUrl = await getImage(extractWeatherKeywords(forecast.current));
    } catch (error) {
      bgUrl = this.state.bgUrl;
    }

    this.appRef.current.style.setProperty('--bg-image-url', `url(${bgUrl}`);
    this.setState({ location, forecast, bgUrl, contentLoading: false });

    setInterval(
      async () =>
        this.setState({
          location,
          forecast: await getForecast(
            this.state.location.longitude,
            this.state.location.latitude,
            this.state.context.language,
          ),
        }),
      60 * 60 * 1000,
    );
  }

  componentDidUpdate() {
    this.appRef.current.style.setProperty('--bg-image-url', `url(${this.state.bgUrl}`);
  }

  updateBg(bgUrl) {
    this.setState({ bgUrl });
  }

  async updateLanguage(language) {
    localStorage.setItem('language', language);
    const { longitude, latitude, city } = this.state.location;
    const forecast = await getForecast(longitude, latitude, language);

    const location = await getLocationByCity(city, language);

    this.setState((state) => ({ context: { ...state.context, language }, forecast, location }));
  }

  updateUnits(units) {
    localStorage.setItem('units', units);
    this.setState({ units });
  }

  async updateLocation(location) {
    this.setState({
      location,
      forecast: await getForecast(location.longitude, location.latitude, this.state.context.language),
    });
  }

  render() {
    const {
      forecast,
      units,
      location: { country, city, latitude, longitude, timezoneOffset },
      context,
      contentLoading,
    } = this.state;

    return (
      <div ref={this.appRef} className="App">
        <div className="App-wrapper">
          <Spinner visible={contentLoading} />
          <BgSwitch
            className="App-bgSwitch"
            keywords={extractWeatherKeywords(forecast.current)}
            onBg={(bgUrl) => this.updateBg(bgUrl)}
          />
          <LanguageSwitch
            className="App-languageSwitch"
            onLanguage={(language) => this.updateLanguage(language)}
            context={context}
          />
          <UnitSwitch className="App-unitSwitch" units={units} onUnits={(newUnits) => this.updateUnits(newUnits)} />
          <LocationSearch
            className="App-locationSearch"
            onLocation={(location) => this.updateLocation(location)}
            context={context}
          />
          <LocationInfo className="App-locationInfo" country={country} city={city} timezoneOffset={timezoneOffset} />
          <LocationMap className="App-locationMap" latitude={latitude} longitude={longitude} context={context} />
          <Weather className="App-weather" forecast={forecast} units={units} context={context} />
        </div>
      </div>
    );
  }
}
