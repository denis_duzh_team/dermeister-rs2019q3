import RS from '../RS';

import { getOffsetDate } from '../../utils/DateUtils';

import './LocationInfo.scss';

export default class LocationInfo extends RS.Component {
  constructor(props) {
    super(props);

    this.state = {
      date: new Date(),
    };
  }

  componentDidMount() {
    setInterval(() => {
      this.setState({ date: new Date() });
    }, 60000);
  }

  render() {
    const offsetDate = getOffsetDate(this.state.date, this.props.timezoneOffset);
    return (
      <div className={`${this.props.className} LocationInfo`}>
        {this.props.city}, {this.props.country},{' '}
        {offsetDate
          .getHours()
          .toString()
          .padStart(2, '0')}
        :
        {offsetDate
          .getMinutes()
          .toString()
          .padStart(2, '0')}
      </div>
    );
  }
}
