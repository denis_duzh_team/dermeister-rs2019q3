const i18n = {
  FEELS_LIKE: {
    en: 'Feels like',
    ru: 'Ощущается',
    be: 'Адчуваецца',
  },
  WIND: {
    en: 'Wind',
    ru: 'Ветер',
    be: 'Вецер',
  },
  WIND_SPEED: {
    en: 'm/s',
    ru: 'м/с',
    be: 'м/с',
  },
  HUMIDITY: {
    en: 'Humidity',
    ru: 'Влажность',
    be: 'Вільготнасць',
  },
};

export default (language) => ({
  FEELS_LIKE: i18n.FEELS_LIKE[language],
  WIND: i18n.WIND[language],
  WIND_SPEED: i18n.WIND_SPEED[language],
  HUMIDITY: i18n.HUMIDITY[language],
});
