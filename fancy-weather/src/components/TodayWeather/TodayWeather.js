import RS from '../RS';

import toI18n from './i18n';

import { getDegrees } from '../../utils/WeatherUtils';
import getLocale from '../../utils/LocationUtils';

import './TodayWeather.scss';

export default class TodayWeather extends RS.Component {
  constructor(props) {
    super(props);

    this.ref = RS.createRef();
  }

  componentDidMount() {
    if (this.props.image) this.ref.current.style.setProperty('--bg-image-url', `url(${this.props.image})`);
  }

  componentDidUpdate() {
    if (this.props.image) this.ref.current.style.setProperty('--bg-image-url', `url(${this.props.image})`);
  }

  render() {
    const { date, degrees, description, feels, wind, humidity } = this.props.weather;
    const { FEELS_LIKE, WIND, WIND_SPEED, HUMIDITY } = toI18n(this.props.context.language);

    return (
      <div className={`${this.props.className} TodayWeather`} ref={this.ref}>
        <p className="TodayWeather-date">
          {date.toLocaleString(getLocale(this.props.context.language), {
            weekday: 'long',
            year: 'numeric',
            month: 'long',
            day: 'numeric',
          })}
        </p>
        <p className="TodayWeather-degrees">{Math.round(getDegrees(degrees, this.props.units)).toString()}</p>
        <div className="TodayWeather-wrapper">
          <p className="TodayWeather-info">{description}</p>
          <p className="TodayWeather-info">
            {FEELS_LIKE}: {Math.round(getDegrees(feels, this.props.units)).toString()}
          </p>
          <p className="TodayWeather-info">
            {WIND}: {Math.floor(wind).toString()} {WIND_SPEED}
          </p>
          <p className="TodayWeather-info">
            {HUMIDITY}: {humidity}%
          </p>
        </div>
      </div>
    );
  }
}
