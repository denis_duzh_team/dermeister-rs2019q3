import mapboxgl from 'mapbox-gl/dist/mapbox-gl';
import RS from '../RS';

import toI18n from './i18n';

import './LocationMap.scss';

export default class LocationMap extends RS.Component {
  constructor(props) {
    super(props);
    mapboxgl.accessToken = 'pk.eyJ1IjoiZGR1emgiLCJhIjoiY2s0MW12NTFyMDJvazNsa2tlbHNkYW9kYyJ9.sr08qYG5zC2dGBtTMljZig';

    this.mapRef = RS.createRef();
  }

  componentDidMount() {
    const { longitude, latitude } = this.props;

    setTimeout(() => {
      this.map = new mapboxgl.Map({
        container: this.mapRef.current,
        style: 'mapbox://styles/mapbox/streets-v11',
        center: [longitude, latitude],
        zoom: 9,
      });
    }, 0);
  }

  componentDidUpdate() {
    this.map.setCenter([this.props.longitude, this.props.latitude]);
  }

  render() {
    const { longitude, latitude } = this.props;
    const { LONGITUDE, LATITUDE } = toI18n(this.props.context.language);

    return (
      <div className={`${this.props.className}`}>
        <div className="LocationMap-map">
          <div ref={this.mapRef}></div>
        </div>
        <p className="LocationMap-coords">
          {LONGITUDE}: {longitude.toFixed(2)}
        </p>
        <p className="LocationMap-coords">
          {LATITUDE}: {latitude.toFixed(2)}
        </p>
      </div>
    );
  }
}
