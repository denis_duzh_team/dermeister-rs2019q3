const i18n = {
  LONGITUDE: {
    en: 'Longitude',
    ru: 'Долгота',
    be: 'Даўгата',
  },
  LATITUDE: {
    en: 'Latitude',
    ru: 'Широта',
    be: 'Шырата',
  },
};

export default (language) => ({
  LONGITUDE: i18n.LONGITUDE[language],
  LATITUDE: i18n.LATITUDE[language],
});
