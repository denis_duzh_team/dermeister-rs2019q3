import RS from '../RS';

import fetchByKeywords from '../../services/ImageService';

import './BgSwitch.scss';

export default class BgSwitch extends RS.Component {
  async onClick() {
    let bgUrl;
    try {
      bgUrl = await fetchByKeywords(this.props.keywords);
      this.props.onBg(bgUrl);
    } catch (error) {}
  }

  render() {
    return (
      <button className={`${this.props.className} BgSwitch`} onclick={() => this.onClick()}>
        Change background
      </button>
    );
  }
}
