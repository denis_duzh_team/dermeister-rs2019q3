import RS from '../RS';

import './UnitSwitch.scss';

const unitsConfig = [
  {
    title: 'C',
    value: 'c',
  },
  {
    title: 'F',
    value: 'f',
  },
];

export default class UnitSwitch extends RS.Component {
  onChange(e) {
    this.props.onUnits(e.target.value);
  }

  isChecked(units) {
    return units === this.props.units;
  }

  render() {
    return (
      <div className={`${this.props.className} UnitSwitch`}>
        {unitsConfig.map((units) => (
          <div className="UnitSwitch-item">
            <input
              type="radio"
              name="units"
              id={units.value}
              value={units.value}
              checked={this.isChecked(units.value)}
              onchange={(e) => this.onChange(e)}
            />
            <label htmlFor={units.value}>{units.title}</label>
          </div>
        ))}
      </div>
    );
  }
}
