import { getDayOfWeek, getSeason, getPeriod } from './DateUtils';

describe('getDayOfWeek()', () => {
  it('should return day of week', () => {
    const date = new Date(Date.parse('12.15.2019'));
    expect(getDayOfWeek(date)).toBe('Monday');
  });

  it('should return undefined', () => {
    const date = new Date(Date.parse('99.15.2019'));
    expect(getDayOfWeek(date)).toBeUndefined();
  });
});

describe('getSeason()', () => {
  it('should return season', () => {
    const date = new Date(Date.parse('12.15.2019'));
    expect(getSeason(date)).toBe('Winter');
  });

  it('should return undefined', () => {
    const date = new Date(Date.parse('99.15.2019'));
    expect(getSeason(date)).toBeUndefined();
  });
});

describe('getPeriod()', () => {
  it('should return day of week', () => {
    const date = new Date(Date.parse('12.15.2019 21:00'));
    expect(getPeriod(date)).toBe('Evening');
  });

  it('should return day of week', () => {
    const date = new Date(Date.parse('99.15.2019'));
    expect(getPeriod(date)).toBeUndefined();
  });
});
