import { getSeason, getPeriod } from './DateUtils';

export const extractWeatherKeywords = ({ date }) => [getSeason(date), getPeriod(date)];

const extractWeather = (apiWeather) => ({
  degrees: apiWeather.main.temp,
  description: apiWeather.weather[0].description,
  wind: apiWeather.wind.speed,
  date: new Date(apiWeather.dt_txt),
  humidity: apiWeather.main.humidity,
  feels: apiWeather.main.feels_like,
  image: apiWeather.weather[0].icon,
});

export const extractForecast = (api) => ({
  current: extractWeather(api.list[0]),
  future: [extractWeather(api.list[5]), extractWeather(api.list[13]), extractWeather(api.list[21])],
});

export const getDegrees = (degrees, units) => (units === 'f' ? (degrees * 9) / 5 + 32 : degrees);
