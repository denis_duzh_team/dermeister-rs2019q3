import { getDegrees } from './WeatherUtils';

describe('getDegrees()', () => {
  it('should return fahrenheit', () => {
    expect(getDegrees(5, 'f')).toBe(41);
  });

  it('should return celsius', () => {
    expect(getDegrees(5, 'c')).toBe(5);
  });
});
