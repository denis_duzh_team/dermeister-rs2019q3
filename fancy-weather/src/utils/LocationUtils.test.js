import getLocale from './LocationUtils';

describe('getLocale()', () => {
  it('should return locale', () => {
    expect(getLocale('en')).toBe('en');
    expect(getLocale('ru')).toBe('ru');
  });

  it('should return undefined', () => {
    expect(getLocale('no-location')).toBeUndefined();
  });
});
