const days = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'];

export const getDayOfWeek = (date) => days[date.getDay()];

export const getSeason = (date) => {
  switch (date.getMonth()) {
    case 0:
    case 1:
    case 11:
      return 'Winter';
    case 2:
    case 3:
    case 4:
      return 'Spring';
    case 5:
    case 6:
    case 7:
      return 'Summer';
    case 8:
    case 9:
    case 10:
      return 'Autumn';
    default:
  }
};

export const getPeriod = (date) => {
  switch (date.getHours()) {
    case 6:
    case 7:
    case 8:
    case 9:
      return 'Morning';
    case 10:
    case 11:
    case 12:
    case 13:
    case 14:
    case 15:
    case 16:
    case 17:
      return 'Day';
    case 18:
    case 19:
    case 20:
    case 21:
      return 'Evening';
    case 22:
    case 23:
    case 0:
    case 1:
    case 2:
    case 3:
    case 4:
    case 5:
      return 'Night';
    default:
  }
};

export const getOffsetDate = (date, timezoneOffset) => {
  const currentOffset = new Date().getTimezoneOffset() * 60000;
  return new Date(date.getTime() + currentOffset + timezoneOffset);
};
