export const getLocationByCity = (city, language) =>
  fetch(
    `https://api.opencagedata.com/geocode/v1/json?q=${city}&language=${language}&key=1eb34a9a9f0b4771bbaf427121ad8a52`,
  )
    .then((res) => res.json())
    .then(({ results: [{ components, geometry, annotations }] }) => ({
      city: components.city || components.state,
      country: components.country,
      latitude: geometry.lat,
      longitude: geometry.lng,
      timezoneOffset: annotations.timezone.offset_sec * 1000,
    }));

export const getLocationByCoords = (longitude, latitude, language) =>
  fetch(
    `https://api.opencagedata.com/geocode/v1/json?q=${latitude}+${longitude}&language=${language}&key=1eb34a9a9f0b4771bbaf427121ad8a52`,
  )
    .then((res) => res.json())
    .then(({ results: [{ components, geometry, annotations }] }) => ({
      city: components.city || components.state,
      country: components.country,
      latitude: geometry.lat,
      longitude: geometry.lng,
      timezoneOffset: annotations.timezone.offset_sec * 1000,
    }));
