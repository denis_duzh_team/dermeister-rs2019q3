import { extractForecast } from '../utils/WeatherUtils';

export default (longitude, latitude, language) =>
  fetch(
    `https://api.openweathermap.org/data/2.5/forecast?lat=${latitude}&lon=${longitude}&lang=${language}&units=metric&appid=720925231a4a01c3fb086d737c8bc303`,
  )
    .then((res) => res.json())
    .then((api) => extractForecast(api));
