export default (keywords) => {
  const url = `https://api.unsplash.com/photos/random?query=${keywords.join(
    ',',
  )}&client_id=9b949d9b2c7d6747c718c9618c9cf4c40f431544d9a811b073fe6a0124439365`;

  return fetch(url)
    .then((res) => res.json())
    .then((image) => image.urls && image.urls.full);
};
