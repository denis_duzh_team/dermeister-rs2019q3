const Node = require("./node");

class LinkedList {
  constructor() {
    this._head = new Node();
    this._tail = new Node();
    this.length = 0;
  }

  lastNode() {
    let current = this._head;
    while (current.next) current = current.next;
    return current;
  }

  append(data) {
    if (!this._head.data) {
      this._head.data = data;
    } else {
      const current = this.lastNode();
      current.next = new Node(data, current);
    }
    this.length++;

    return this;
  }

  head() {
    return this._head.data;
  }

  tail() {
    return this.lastNode().data;
  }

  at(index) {
    let current = this._head;
    for (let i = 0; i < index; i++) {
      current = current.next;
    }
    return current.data;
  }

  insertAt(index, data) {
    let current = this._head;
    for (let i = 0; i < index; i++) {
      current = current.next;
    }
    const newNode = new Node(data, current.prev, current);
    if (current.prev) current.prev.next = newNode;
    current.prev = newNode;

    if (index === 0) {
      this._head = !this._head.data ? new Node(data) : newNode;
    }

    return this;
  }

  isEmpty() {
    return this.length === 0;
  }

  clear() {
    this._head = new Node();
    this.length = 0;

    return this;
  }

  deleteAt(index) {
    if (index === 0) {
      this._head = !this._head.next ? new Node() : this._head.next;
    } else {
      let current = this._head;
      for (let i = 0; i < index; i++) current = current.next;
      current.prev.next = current.next;
    }
    return this;
  }

  reverse() {
    let current = this._head;
    while (current) {
      const buffer = current.next;
      current.next = current.prev;
      current.prev = buffer;

      this._head = current;
      current = current.prev;
    }
    return this;
  }

  indexOf(data) {
    let current = this._head;
    let index = 0;
    while (current && current.data !== data) {
      index++;
      current = current.next;
    }
    return current ? index : -1;
  }
}

module.exports = LinkedList;
