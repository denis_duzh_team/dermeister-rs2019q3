# React

## Slide 1
React is a javascript library that simplifies the way you build user interfaces.

## Slide 1-1
It is developed and maintained by Facebook Open Source.

## Slide 2-1
We are going to look at core React concepts on the example of virtual keyboard application that is actually a simplified version of one of our previous codejams.

## Slide 2-2
First of all, React introduces the concept of a component.
Component is a single block of UI that holds inside specified behavior and appearance.
It can be anything from a single button to the whole application.
Components are designed to be reused in the future, so practically components are independent of each other.
You can think of them to be similar to BEM blocks.
(reference to the slide)

## Slide 2-3
Here you can see that our virtual keyboard has two types of components: a single key and the keyboard
So it is pretty obvious that each key would have almost identical behavior and appearance.
The same applies to the keyboard.

## Slide 3-1
(reference to the slide)
From the point of coding, a component is a javascript class that extends Component class imported from React library.

## Slide 3-2
The only thing required is that your class implements ```render()``` method.
This method must return an object that basically describes how your component should look like.
This object is created using ```createElement()``` function imported from React library as well and it represents HTML element that corresponds to our component.
(reference to slide)
```createElement()``` receives HTMl tag name, its attributes and what should be put inside our HTML element.
This also works for nested HTML elements.
This also works for other components, so instead of providing tag name you provide javascript class that stands for our component.
However, describing component structure this way turned out to be quite inconvenient.
That is why welcome JSX.

## Slide 3-3
JSX stands for Javascript Extended and it allows you to write such beautiful constructions in the HTML-like syntax.
Eventually this JSX gets transformed into nested calls of ```createElement()``` function so this is just a syntax sugar for what we have seen before.

## Slide 4-1
Let's look at how we can implement our Keyboard component.
(reference to slide)
First of all, we define Keyboard class and extend React.Component class.
Second of all, we declare ```render()``` method and return simple ```<div>``` from it

## Slide 4-2
You may be wondering how to apply our component to real DOM.
It is actually very simple.
All we need is to use ```ReactDOM.render()``` function and pass there our root component, in our case it would be ```<Keyboard />```
And we are done, our component is rendered to DOM.

## Slide 5-1
For now, our keyboard is a single div with no content and we want to fill it with keys.
For this case, we need to create one more component, that would be our ```<Key />```. Though let's defer implementing it and pretend we have already done it and get to it later. We just use it in ```render()``` method the same way as ```<div>```.
It may look like this(reference to slide).

## Slide 5-2
So, since we use components alongside with HTML elements, it is reasonable to expect that we can specify attributes and event handlers for our components.
For instance, Key component may probably need ```value``` attribute and ```onClick``` handler.
So we just write it this way(reference to slide).

## Slide 6-1
Now it's time we look inside the Key component and explore it. As we see this component is defined the same way Keyboard is. In its render method we describe how our key should be looking like. But the main interest for us here is how to access ```value``` and ```onClick``` that were passed from Keyboard. And it is done pretty easily.

## Slide 6-2
Every time a new component is instantiated by React it receives ```props``` object passed as a parameter to its constructor. That is why you need to define component constructor considering this parameter and call ```super()```.

## Slide 6-3
So in order to access ```value``` and ```onClick``` you need to reference ```this.props.value``` and ```this.props.onClick``` accordingly and operate them just as regular javascript values and functions. 
This core concept of React is called Props. So we can now display our key's title and set up click event.

## Slide 7-1
By now our keyboard is able to respond to clicks and currently logs its value to the console. Next we are going to make our keyboard multi-language. 
For this we need to introduce the next core React concept. It's name is the State. The idea of the state is incredibly simple. Here we have Keyboard component which can have as it's state many properties.
For instance, current language, currently pressed buttons and so on, basically features of the component that change over time.

## Slide 7-2
In React state is just a field of component class. It is initialized in constructor and referred by ```this.state```. 

## Slide 7-3
Let's set initial language to English. 

## Slide 7-4
Also we will pretend we have global array of objects that describe each key. It may look like this. 

## Slide 7-5
We see that it defines values of a key depending on a language. So we are to modify our code to look like this. Now we have bound Key props with Keyboard state.

## Slide 7-6
Yet our keyboard remains static and we would wish to change the language of the keyboard and so that each key changes it's value respectively.
To achieve that we put here a simple button with an event handler. In the future we can switch to shortcut instead. So in the button's event handler we call ```this.setState()``` method that is extended from React.Component. 
Invoking this method causes Keyboard re-render itself. Under the hood ```setState``` merges passed object with components state field thus updating it's state and then it invokes ```render()``` method. 
This leads to re-calculating of child component's props and after all we have them synchronized with components state.
Also React takes care of setting real dom nodes values to match its React elements.

## Slide 8
This is it. We have built a simple keyboard that logs pressed buttons to console and is able to change language. It is just as easy to extend it with features that were required by codejam such as shortcuts, shift and caps lock.