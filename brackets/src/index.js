module.exports = function check(str, bracketsConfig) {
    const map = new Map(bracketsConfig);
    const buffer = [];

    str.split("").forEach(bracket =>
        bracket === map.get(buffer[buffer.length - 1]) ? buffer.pop() : buffer.push(bracket)
    );

    return buffer.length === 0;
};
